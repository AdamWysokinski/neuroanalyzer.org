---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Remove power line noise

Power line noise (at 50 or 60 Hz, depending on the country) may be automatically attenuated using `remove_powerline()`. Currently it supports:

- IIR notch filtering + automated detection and removal of power line peaks; for best results the notch filter bandwidth is calculated separately for each peak for each channel (this may take approx. 2 minutes per object)

Detect power line noise frequency using linear regression method:
```julia
noise_frq = detect_powerline(eeg)
```

(!) The detection algorithm is based a series of linear regression models that estimates the relationships between the input signal and the sum of sine and cosine waveforms of consecutive frequencies (from 1 Hz to the input signal Nyquist frequency). For each model the sum of squared coefficients for the sine and cosine components of the model is used to calculate the noise power. Noise frequency is determined from the frequency that produces the highest power in these models. This experimental modeling is based on `GLM.lm()` and requires further validations.

The output is in Hz and shaped as channels × epochs.

To use IIR notch filtering method:
```julia
eeg = import_edf("NEURO-testing-data/EDF/eeg-test-edf.edf")
p1 = plot_psd(eeg, ch=1)
remove_powerline!(eeg, method=:iir, pl_frq=50)
p2 = plot_psd(eeg, ch=1)
p = Plots.plot(p1, p2, layout=(2, 1))
plot_save(p, file_name="images/remove_pl1.png")
```

(!) This method is based on analyzing a series of IIR notch filters of various bandwidths. The optimal bandwidth is selected using the filtered signal. The variance within the window of peak power line noise frequency ±5.0 Hz (default value) is calculated for each peak and the IIR notch filter bandwidth that gives the lowest variance is used for noise filtering. Next, the procedure is repeated for other detected peaks of defined prominence (default is 2.0 dB). This is and experimental modeling and requires further validations.

Output:

    [ Info: Removing power line noise at 50 Hz and its harmonics
    Progress: 100%|████████████████████| Time: 0:01:46         
    24×6 DataFrame
     Row │ channel  power line bandwidth  peak 1 frequency  peak 2 frequency  peak 1 bandwidth  peak 2 bandwidth 
         │ Int64    Float64               Float64           Float64           Float64           Float64          
    ─────┼───────────────────────────────────────────────────────────────────────────────────────────────────────
       1 │       1                   1.6             100.0             106.0               0.2               0.2
       2 │       2                   1.3             100.0             106.0               0.2               0.2
       3 │       3                   3.4             100.0             106.0               0.3               0.3
       4 │       4                   2.2             100.0             106.0               0.2               0.2
       5 │       5                   3.1             100.0             106.0               0.3               0.3
       6 │       6                   3.5             100.0             106.0               0.3               0.3
       7 │       7                   1.9             100.0             106.0               0.2               0.3
       8 │       8                   1.6             100.0             106.0               0.2               0.2
       9 │       9                   0.1             100.0             106.0               0.3               5.0
      10 │      10                   1.9             100.0             106.0               0.2               0.2
      11 │      11                   5.0             100.0             106.0               0.2               0.2
      12 │      12                   0.7             100.0             106.0               0.1               0.1
      13 │      13                   1.8             100.0             106.0               0.2               5.0
      14 │      14                   0.9             100.0             106.0               0.2               0.1
      15 │      15                   2.2             100.0             106.0               0.2               0.2
      16 │      16                   1.2             100.0             106.0               0.2               0.2
      17 │      17                   3.1             100.0             106.0               0.3               0.3
      18 │      18                   1.6             100.0             106.0               0.1               0.2
      19 │      19                   2.7             100.0             106.0               0.3               0.3
      20 │      20                   4.2             100.0             106.0               0.2               0.1
      21 │      21                   3.6             100.0             106.0               0.2               0.2
      22 │      22                   5.0             100.0             106.0               0.1               0.1
      23 │      23                   5.0             100.0             106.0               0.2               0.2
      24 │      24                   5.0             100.0             106.0               0.3               0.2

![](images/remove_pl1.png)

The output data frame may be used to remove peaks manually:
```julia
NeuroAnalyzer.filter!(eeg, ch=1, fprototype=:iirnotch, cutoff=50, bw=1.6))
NeuroAnalyzer.filter!(eeg, ch=1, fprototype=:iirnotch, cutoff=100, bw=0.2))
NeuroAnalyzer.filter!(eeg, ch=1, fprototype=:iirnotch, cutoff=106, bw=0.2))
```

```julia
eeg = import_edf("NEURO-testing-data/EDF/eeg-test-edf.edf")
e10 = epoch(eeg, ep_len=10)
p1 = plot_spectrogram(e10, ch=1, ep=2)
remove_powerline!(eeg, method=:iir, pl_frq=50)
e10 = epoch(eeg, ep_len=10)
p2 = plot_spectrogram(e10, ch=1, ep=2)
p = Plots.plot(p1, p2, layout=(2, 1))
plot_save(p, file_name="images/remove_pl2.png")
```

![](images/remove_pl2.png)