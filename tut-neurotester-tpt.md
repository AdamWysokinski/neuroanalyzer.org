---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroTester: Two-point Pinch Test (TPT)

Two-point Pinch Test (TPT) is variant of Finger Tapping Test (FTT), where instead of tapping a subject is asked to perform two-point (thumb and index finger) pinches as quickly as possible.

TPT is measured using [Grove - 3-Axis Digital Accelerometer](https://wiki.seeedstudio.com/Grove-3-Axis_Digital_Accelerometer-1.5g/) via Arduino attached to the PC via USB (virtual serial port). The Groove GSR sensor should be placed on the index finger. We use an elastic band to which the sensor is attached.

The [daemon](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/src/branch/main/misc/tpt.ino) must be running on Arduino in order to record, convert and send the data to the serial port. Data from the sensor are sampled at 50 Hz.

## tpt() and itpt()

`tpt()` and `itpt()` parameters are:

- `duration`: recording length (in seconds)
- `port_name`: serial port name

To record TPT in GUI mode:
```julia
e = itpt(duration=60, port_name="/dev/ttyUSB0")
```

![](images/itpt.png)

To record TPT in CLI mode:
```julia
e = tpt(duration=60, port_name="/dev/ttyUSB0")
```

Output:
```
   NeuroTester: TPT
   ================
      Duration: 5 [seconds]
   Serial port: /dev/ttyUSB0

   Ready to start, press SPACEBAR to begin the test

   The test will start after a beep

      Pinch the thumb and the index finger as quickly as possible

   Testing completed
```

(!) `"tpt"` datatype NeuroAnalyzer object is returned.

Example of TPT recorded signal:

```julia
iview(e)
```

![](images/tpt.png)

To analyze the test results, use:
```julia
tpt_analyze(e)
```
