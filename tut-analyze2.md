---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Analyze EEG (2)

Frequencies, Nyquist frequency:
```julia
hz, nyq = freqs(eeg)
```

Calculate signal total power:
```julia
total_power(eeg)
```

Calculate band power:
```julia
band_power(eeg, frq_lim=(8, 12.5))
```

Calculate maximum frequency of a band power:
```julia
_, mfrq, _ = band_mpower(eeg, ch=get_channel(eeg, type="eeg"), frq_lim=band_frq(eeg, band=:alpha))
p = plot_bar(mfrq[1:19, 1], xlabels=labels(eeg)[1:19], ylabel="Frequency [Hz]", title="Maximum α band frequency\n[epoch: 1]")
plot_save(p, file_name="images/alpha-maxfrq.png")
```
![](images/alpha-maxfrq.png)
