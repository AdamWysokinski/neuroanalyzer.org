---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Plot (2)

Plot PSD (Welch periodogram):
```julia
p = plot_psd(eeg, ch=1)
plot_save(p, file_name="images/psd.png")
```
![](images/psd.png)

Plot PSD (FFT):
```julia
p = plot_psd(eeg, ch=1, method=:fft)
plot_save(p, file_name="images/psd_st.png")
```
![](images/psd_st.png)

Plot PSD (short time Fourier transform):
```julia
p = plot_psd(eeg, ch=1, method=:stft)
plot_save(p, file_name="images/psd_stft.png")
```
![](images/psd_stft.png)

Plot PSD (multi-taper):
```julia
p = plot_psd(eeg, ch=1, method=:mt)
plot_save(p, file_name="images/psd_mt.png")
```
![](images/psd_mt.png)

Plot PSD (Morlet-wavelet):
```julia
p = plot_psd(eeg, ch=1, method=:mw)
plot_save(p, file_name="images/psd_mw.png")
```
![](images/psd_mw.png)

Plot PSD, scale x-axis:
```julia
p = plot_psd(eeg, ch=1, ax=:loglin)
plot_save(p, file_name="images/psd_loglin.png")
```
![](images/psd_loglin.png)

Plot multi-channel PSD:
```julia
p = plot_psd(eeg, ch=1:4)
plot_save(p, file_name="images/psd_mch1.png")
```
![](images/psd_mch1.png)

Butterfly plot of multi-channel PSD:
```julia
p = plot_psd(eeg, ch=1:4, type=:butterfly)
plot_save(p, file_name="images/psd_mch2.png")
```
![](images/psd_mch2.png)

Plot multi-channel PSD, mean ± 95CI:
```julia
p = plot_psd(eeg, ch=1:4, type=:mean)
plot_save(p, file_name="images/psd_avg.png")
```
![](images/psd_avg.png)

Plot PSD 3d waterfall:
```julia
p = plot_psd(eeg, ch=1:4, method=:mw, type=:w3d)
plot_save(p, file_name="images/eeg_psdw3d.png")
```
![](images/eeg_psdw3d.png)

Plot PSD 3d surface:
```julia
p = plot_psd(eeg, ch=1:4, type=:s3d)
plot_save(p, file_name="images/eeg_psds3d.png")
```
![](images/eeg_psds3d.png)

Plot topomap of PSDs:
```julia
p = plot_psd(eeg, ch=1:19, type=:topo)
plot_save(p, file_name="images/eeg_psd_topo.png")
```

![](images/eeg_psd_topo.png)

Plot phase spectral density:
```julia
p = plot_phsd(eeg, ch=1:19)
plot_save(p, file_name="images/eeg_phsd.png")
```

![](images/eeg_phsd.png)

(!) For `plot_phsd()`, the same plot types are available as for `plot_psd()`.