---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Analyze EEG (1)

Any analysis data (e.g. ICA, PCA) can be stored within the EEG object as EEG component.

For more details, see [`add_component()`](https://neuroanalyzer.org/docs/#NeuroAnalyzer.add_component), [`delete_component()`](https://neuroanalyzer.org/docs/#NeuroAnalyzer.delete_component), [`rename_component()`](https://neuroanalyzer.org/docs/#NeuroAnalyzer.rename_component), [`component_type()`](https://neuroanalyzer.org/docs/#NeuroAnalyzer.component_type), [`extract_component()`](https://neuroanalyzer.org/docs/#NeuroAnalyzer.extract_component).

(!) Components are removed from the object when the signal data is altered in any way (e.g. channel removal, filtering).

(!) Components are saved together with the signal data when using the native NeuroAnalyzer format (HDF5).

Add component:
```julia
e = epoch_stats(eeg)
add_component!(eeg, c=:epoch_mean, v=e[1])
```

Show components:
```julia
list_components(eeg)
```

Get component type:
```julia
component_type(eeg, c=:epoch_mean)
```

Get data of the embedded component:
```julia
extract_component(eeg, c=:epoch_mean)
```

Delete component:
```julia
delete_component!(eeg, c=:epoch_mean)
```

Delete all components:
```
reset_components!(eeg)
```