---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: ECoG

Currently, ECoG functionality is very limited.

Load ECoG data:
```julia
using MAT
file_name = "bp_mot_t_h.mat"
dataset = matread(file_name)
data = Float64.(reshape(dataset["data"]', 47, :, 1)) # channels must be by rows
ecog = create(data_type="ecog")
add_channel!(ecog, data=data, type=repeat(["ecog"], 47), unit=repeat(["µV"], 47))
create_time!(ecog, fs=1000)                          # the dataset sampling rate is 1000 Hz
```

Show signal details:
```julia
info(ecog)
```

Output:

                  Data type: ECOG
                File format: 
                Source file: 
             File size [MB]: 0
           Memory size [MB]: 140.72
                    Subject:  
                  Recording: 
            Recording notes: 
             Recording date: 
             Recording time: 
         Sampling rate (Hz): 1000
    Signal length [samples]: 376400
    Signal length [seconds]: 376.4
         Number of channels: 47
           Number of epochs: 1
     Epoch length [samples]: 376400
     Epoch length [seconds]: 376.4
                     Labels: yes
                    Markers: no
          Channel locations: no
                 Components: no
    Channels:
     ch     label           type        unit    
     1      1               ECOG        µV
     2      2               ECOG        µV
     3      3               ECOG        µV
     4      4               ECOG        µV
     5      5               ECOG        µV
     6      6               ECOG        µV
     7      7               ECOG        µV
     8      8               ECOG        µV
     9      9               ECOG        µV
     10     10              ECOG        µV
     11     11              ECOG        µV
     12     12              ECOG        µV
     13     13              ECOG        µV
     14     14              ECOG        µV
     15     15              ECOG        µV
     16     16              ECOG        µV
     17     17              ECOG        µV
     18     18              ECOG        µV
     19     19              ECOG        µV
     20     20              ECOG        µV
     21     21              ECOG        µV
     22     22              ECOG        µV
     23     23              ECOG        µV
     24     24              ECOG        µV
     25     25              ECOG        µV
     26     26              ECOG        µV
     27     27              ECOG        µV
     28     28              ECOG        µV
     29     29              ECOG        µV
     30     30              ECOG        µV
     31     31              ECOG        µV
     32     32              ECOG        µV
     33     33              ECOG        µV
     34     34              ECOG        µV
     35     35              ECOG        µV
     36     36              ECOG        µV
     37     37              ECOG        µV
     38     38              ECOG        µV
     39     39              ECOG        µV
     40     40              ECOG        µV
     41     41              ECOG        µV
     42     42              ECOG        µV
     43     43              ECOG        µV
     44     44              ECOG        µV
     45     45              ECOG        µV
     46     46              ECOG        µV
     47     47              ECOG        µV

Plot signal:
```julia
p = NeuroAnalyzer.plot(ecog)
plot_save(p, file_name="images/ecog.png")
```

![](images/ecog.png)