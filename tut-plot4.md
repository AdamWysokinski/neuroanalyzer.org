---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Plot (4)

```julia
p1 = NeuroAnalyzer.plot(eeg, ch=1, ep=1)
p2 = plot_psd(eeg, ch=1, ep=1)
p3 = plot_spectrogram(eeg, ch=1, ep=1, frq_lim=(0, 40))
p = plot_compose([p1, p2, p3], layout=(3, 1), size=(1200, 900))
plot_save(p, file_name="images/eeg_complex1.png")
```

![eeg complex1](images/eeg_complex1.png)

It is also possible to combine different plots using complex layout:
```julia
p1 = plot(eeg, ch=1:2)
p2 = plot_psd(eeg, ch=1, ep=1)
p3 = plot_locs(eeg, ch=1:19, selected=1:2, labels=false, large=false)

# @layout macro requires Plots package to be loaded
p = Plots.plot(p1, p2, p3, plot_size=(1200, 600), layout=@layout [a b; _ c{0.5h} _])
plot_save(p, file_name="images/complex_layout.png")
```

![](images/complex_layout.png)

## Tiled plots

As an example, a tiled plot of labeled auto-covariance plots will be created.

First, we need data to plot:
```julia
ac, lags = acov(eeg, lag=5)
l = labels(eeg)[signal_channels(eeg)]
```

Next, a vector of plots has to be created:
```julia
p = Plots.Plot{Plots.GRBackend}[]
for idx in 1:size(ac, 1)
    push!(p, plot_xac(ac[idx, :], lags, title=l[idx]))
end
```

There is also `plot_compose()` function available:
```julia
plot_compose(p[1:7], layout=(4, 2))
```

This can also be done manually. Create a tiled plot (two columns) of first seven plots. First example is to plot even number of plots in two columns:
```julia
p = p[1:7]
# add empty plot in case of odd number of sub-plots
isodd(length(p)) && push!(p, plot_empty())
p = Plots.plot(p..., layout=(length(p) ÷ 2, 2))
plot_save(p, file_name="images/multi-acov.png")
```

![](images/multi-acov.png)
