---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Analyze EEG (3)

Often it is required to analyze only a segment of data, e.g. part of the spectrogram or topographical map.

First, a matrix data has to be prepared, e.g.:
```julia
# data at 2560 sample across 19 channels
s = eeg.data[1:19, 2560, 1]
# prepare planar interpolation
z, x, y = plinterpolate(s, locs=locs, ch=1:19)
# preview
Plots.heatmap(x, y, z)
```

Next, a segment of interest has to be extracted:
```julia
seg = iselect_seg(z)
```
![](images/iselect_seg.png)

(!) Place the selection corners using left clicks. Middle click undoes the last action. Press `Enter` when ready or `ctrl-q` to cancel and close the window.

Output:

    (35, 37, 63, 63)

These are coordinates of the left upper corner (row and column) and bottom right corner (row and column) of the segment.

To select a circular segment, use `shape=:c` parameter:
```julia
seg = iselect_seg(z, shape=:c)
```

![](images/iselect_segc.png)

For circular segment coordinates are: center (row and column) and peripheral point (row and column).

To extract the segment data:
```julia
m = seg_extract(z, seg)
```

Output:

    29×27 Matrix{Float64}:
     0.449312  …  0.488266
     ⋮         ⋱  ⋮         
     0.640943     0.659986

If you need the segment to be extracted as a vector, use the `v=true` parameter.
```julia
v = seg_extract(z, seg, v=true)
```

Output:

    783-element Vector{Float64}:
     0.44931180669912063
     ⋮
     0.6177368628322506

If segment is circular, use `shape=:c` parameter when extracting: 
```julia
m = seg_extract(z, seg, shape=:c)
```

(!) Circular segments are always returned as vectors.

Alternatively, use `iselect_seg()` with `extract=true` parameter.
```julia
seg = iselect(z, extract=true, v=true)
```

or combine these two functions:
```julia
seg = seg_extract(z, iselect_seg(z))
```