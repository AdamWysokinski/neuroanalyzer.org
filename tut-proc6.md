---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Process EEG (6)

Interpolate bad channel 1, epoch 1:2 using planar interpolation:
```julia
e10pl = plinterpolate_channel(e10, ch=1, ep=1:2)
Plots.plot(e10.data[1, :, 1])
Plots.plot!(e10pl.data[1, :, 1])
```

(!) Planar interpolation requires electrode locations. Only a single channel may be interpolated at the same time. Channel may be interpolated across more than 1 epoch.

(!) This experimental modeling is based on `ScatteredInterpolation.interpolate()` and requires further validations.

Interpolate bad channel 5, epoch 25 using linear regression, use epochs 17:24 as reference:
```julia
e10lr = lrinterpolate_channel(e10, ch=5, ep=25)
iview(e10, e10lr)
```

(!) This experimental modeling is based on `GLM.lm()` and requires further validations.

Output:

    [ Info: Accuracy report:
    [ Info:  R²: 0.853
    [ Info:  RMSE: 0.059
    [ Info:  MAE: 2.884
    [ Info:  AIC: 36.318
    [ Info:  BIC: 174.991

![](images/lrinterpolate.png)

(!) Linear regression interpolation requires ≥ 1 epoch of good signal in interpolated channel, as linear regressor is trained using good epochs. The more epochs with good signal, the better interpolation results. Only a single channel at one epoch may be interpolated at the same time.

Interpolate bad channel 5, epoch 25 using RandomForestRegressor, use epochs 17:24 as reference:
```julia
using MLJ
using MLJDecisionTreeInterface
rfr = @MLJ.load RandomForestRegressor pkg=DecisionTree verbosity=0
rfr_model = rfr(max_depth = -1, 
                min_samples_leaf = 1, 
                min_samples_split = 2, 
                min_purity_increase = 0.0, 
                n_subfeatures = -1, 
                n_trees = 500, 
                sampling_fraction = 1.0, 
                feature_importance = :split)
e10ml = mlinterpolate_channel(e10, ch=5, ep=25, ep_ref=17:24, model=rfr_model)
iview(e10, e10ml)
```

Output:

    [ Info: Training machine(RandomForestRegressor(max_depth = -1, …), …).
    [ Info: Accuracy report:
    [ Info:  R²: 0.9834
    [ Info:  RMSE: 0.4798

![](images/mlinterpolate.png)

(!)  Machine learning-based interpolation requires ≥ 1 epoch of good signal in interpolated channel, as the regressor is trained using good epochs. The more epochs with good signal, the better interpolation results. Only a single channel at one epoch may be interpolated at the same time.