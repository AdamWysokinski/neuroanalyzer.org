---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Animate

Animate:
```julia
using Plots
ts = 0:0.01:10
anim = @animate for idx ∈ 1:length(ts)
    @info "Generating frame: $idx/$(length(ts))"
    plot_topo(eeg, seg=(ts[idx], ts[idx]+0.1), channel=1:19)
end
gif(anim, "images/anim_fps10.gif", fps = 10)
```

Reduce output file size:
```sh
ffmpeg -y -i images/anim_fps10.gif -filter_complex "fps=5,scale=480:-1:flags=lanczos,split[s0][s1];[s0]palettegen=max_colors=32[p];[s1][p]paletteuse=dither=bayer" images/anim_fps10.gif

```

![](images/anim_fps10.gif)