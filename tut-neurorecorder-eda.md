---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroRecorder: Electrodermal Activity (EDA)

Electrodermal Activity (EDA), also known as Galvanic Skin Response (GSR), is variation in the skin conductance.

EDA is measured using [Groove GSR sensor](https://wiki.seeedstudio.com/Grove-GSR_Sensor/) via Arduino attached to the PC via USB (virtual serial port). The two Groove GSR sensor electrodes should be placed on the index and middle fingers on the middle phalanx. The electrode surface should be placed against the inside part of the finger. The distal phalange has more density of sweat glands, although the middle phalange is also a common place used to attach the electrodes because it is less likely that they will fall off the finger [10.1111/j.1469-8986.2012.01384.x](https://doi.org/10.1111/j.1469-8986.2012.01384.x).

Small amount of isotonic saline (NaCl 0.9%) solution can be used to improve the contact between skin and electrodes.

The [daemon](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/src/branch/main/misc/gsr.ino) must be running on Arduino in order to record, convert and send the data to the serial port. Data from the sensor are sampled at 50 Hz.

## edar() and iedar()

`edar()` and `iedar()` parameters are:

- `duration`: recording length (in seconds)
- `port_name`: serial port name

To record EDA in GUI mode:
```julia
e = iedar(duration=60, port_name="/dev/ttyUSB0")
```

![](images/iedar.png)

To record EDA in CLI mode:
```julia
e = edar(duration=60, port_name="/dev/ttyUSB0")
```

Output:
```
   NeuroRecorder: EDA
   ==================
      Duration: 60 [seconds]
   Serial port: /dev/ttyUSB1
   
   Ready to start, press SPACEBAR to begin recording
   
   The recording will start after a beep
   
   Recording ............................................................
   
   Recording finished.
```

(!) `"eda"` datatype NeuroAnalyzer object is returned.

Example of EDA recorded signal:

```julia
plot(e, seg=(0, signal_duration(e)))
```

![](images/eda.png)