---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: EEG study

Create study object:
```julia
eeg1 = filter(eeg, fprototype=:butterworth, ftype=:bs, cutoff=(45, 55), order=4)
eeg2 = filter(eeg, fprototype=:butterworth, ftype=:bs, cutoff=(45, 55), order=8)
eeg3 = filter(eeg, fprototype=:butterworth, ftype=:bs, cutoff=(45, 55), order=12)
my_study = study_create([eeg1, eeg2, eeg3], [:g1, :g2, :g3])
my_study.objects[1].data
```

Save and load study object:
```julia
save_study(my_study, file_name="my_study.hdf")
my_study2 = load_study(file_name="my_study.hdf")
```

(!) Study is saved as [HDF5](https://en.wikipedia.org/wiki/Hierarchical_Data_Format) file. File name extension must be `.hdf`.
