---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Interactive plots

For interactive plots, GTK and Cairo are required (all necessary libraries should be installed automatically).

## Viewing

`iview()` is a multi-tool for viewing and editing continuous and epoched signals:
```julia
iview(eeg)
iview(eeg, zoom=20) # show signal in 20-second fragments
```

![](images/iview.png)

(!) Default `zoom` value is 10 seconds.

(!) Place the mouse cursor over the plot and use the mouse wheel to scroll channels up and down.

(!) Shift + mouse wheel scrolls the signal by one second.

(!) Ctrl + mouse wheel scrolls the signal by one segment (default is 10 seconds).

(!) Left click on the channel label shows its details.

(!) Right click on the channel label marks the channel as bad.

(!) Right click on the `-` button to go to the start of the signal.

(!) Right click on the `+` button to go to the end of the signal.

Keyboard shortcuts:

- `Ctrl + b`: toggle butterfly plot
- `Ctrl + m`: toggle mean plot
- `Home`: go to the signal start
- `End`: go to the signal end
- `Ctrl + ,`: go back by 1 seconds
- `Ctrl + .`: go forward by 1 seconds
- `Alt + ,`: go back by `zoom` seconds
- `Alt + .`: go forward by `zoom` seconds
- `Page Up`: slide channels up
- `Page Down`: slide channels down
- `Alt + m`: toggle monochromatic mode
- `Alt + s`: toggle scales
- `[`: zoom in
- `]`: zoom out
- `Ctrl + h`: show keyboard shortcuts
- `Ctrl + q`: close

Butterfly plot:
![](images/iview_bf.png)

Mean plot:
![](images/iview_avg.png)

(!) Epoched signal should be viewed using `iview_ep()`, while continuous signal should be viewed using `iview()`. The meta-function `iview()` detects type of the signal and chooses appropriate function.

![](images/iview_ep.png)

For epoched signal the keyboard shortcuts are:

- `Ctrl + b`: toggle butterfly plot
- `Ctrl + m`: toggle mean plot
- `Home`: go to the first epoch
- `End`: go to the last epoch
- `Ctrl + ,`: previous epoch
- `Ctrl + .`: next epoch
- `Page Up`: slide channels up
- `Page Down`: slide channels down
- `Alt + m`: toggle monochromatic mode
- `Alt + s`: toggle scales
- `Ctrl + h`: show keyboard shortcuts
- `Ctrl + q`: close

(!) Shift + mouse wheel scrolls the signal by one epoch.

(!) Left click on the channel label will show its details.

(!) Right click on the channel label will mark the channel in the current epoch as bad.

(!) Right click on the `-` button to go to the first epoch.

(!) Right click on the `+` button to go to the last epoch.

To view one channel at a time, use:
`iview(eeg, mch=false)`

![](images/iview_mch.png)

## Editing

To interactively edit the signal:
```julia
iview(eeg)
```

(!) Epoched signal should be edited using `iview_ep()`, while continuous signal should be edited using `iview()`.

![](images/preview_cont.png)

(!) Place the first marker by clicking with the left mouse button and the second marker with the right mouse button or use time segment entry boxes.

Keyboard shortcuts:

- `Ctrl + Enter`: return current time segment
- `Ctrl + d`: delete time segment
- `Ctrl + s`: toggle snapping

(!) By default markers are snapped to time points at the closest quarter (0.25, 0.5, 0.75 or 1.0) of a second; to turn it off, use `snap=false` option or press `Ctrl + s` while editing.

To edit the epoched signal interactively:
```julia
iview(eeg)
```

Keyboard shortcuts:

- `Ctrl + d`: delete current epoch

## Comparing signals

To compare two signals, e.g. before and after filtering:
```julia
iview(eeg1, eeg2)
```

![](images/iplot2.png)

Keyboard shortcuts:

- `Home`: go to the signal start
- `End`: go to the signal end
- `Ctrl + ,`: go back by 1 seconds
- `Ctrl + .`: go forward by 1 seconds
- `Alt + ,`: go back by `zoom` seconds
- `Alt + .`: go forward by `zoom` seconds
- `Page Up`: slide channels up
- `Page Down`: slide channels down
- `Alt + s`: toggle scales
- `[`: zoom in
- `]`: zoom out
- `Ctrl + h`: show keyboard shortcuts
- `Ctrl + q`: close

For epoched signal the keyboard shortcuts are:

- `Home`: go to the first epoch
- `End`: go to the last epoch
- `Ctrl + ,`: previous epoch
- `Ctrl + .`: next epoch
- `Page Up`: slide channels up
- `Page Down`: slide channels down
- `Alt + s`: toggle scales
- `Ctrl + h`: show keyboard shortcuts
- `Ctrl + q`: close

## Viewing components

`iview()` can also be used to preview embedded or external components:
```julia
iview(eeg, ic)
```

## Viewing plots

Each plot object may be opened using an interactive viewer:
```julia
p = plot(eeg, ch=["Fp1", "Fp2", "F3", "F4"], seg=(10, 20))
iview(p)
```

![](images/iview_plot.png)

Available shortcuts:

- `Ctrl + s`: save as PNG file
- `Ctrl + q`: close

`iview()` may also be used to open PNG files:
```julia
iview("images/eeg_complex1.png")
```

## ipsd()

PSD may be plotted using interactive plot:
```julia
ipsd(eeg, ch="eeg")
```

![](images/ipsd.png)

For epoched signal:
```julia
ipsd_ep(e10, ch="eeg")
```

![](images/ipsd_ep.png)

Keyboard shortcuts:

- `Home`: go to the first epoch
- `End`: go to the last epoch
- `Ctrl + ,`: previous epoch
- `Ctrl + .`: next epoch
- `[`: zoom in
- `]`: zoom out
- `Ctrl + s`: save as PNG
- `Ctrl + h`: show keyboard shortcuts
- `Ctrl + q`: close

For continuous wavelet transformation, custom wavelet formula may be entered in the field (default is `Morlet(2π), β=32, Q=128`). Please refer to the documentation of [ContinuousWavelets.jl](https://dsweber2.github.io/ContinuousWavelets.jl/dev/coreType/#Available-Wavelet-Families) to see the list of available wavelet families and parameters.

(!) Shift + mouse wheel scrolls the signal by one second.

(!) Ctrl + mouse wheel scrolls the signal by one segment (default is 10 seconds).

## ispectrogram()

Spectrogram may be plotted using interactive plot:
```julia
ispectrogram(eeg, ch="eeg")
```

![](images/ispectrogram.png)

Keyboard shortcuts:

- `Home`: go to the signal start
- `End`: go to the signal end
- `Ctrl + ,`: go back by 1 seconds
- `Ctrl + .`: go forward by 1 seconds
- `Alt + ,`: go back by `zoom` seconds
- `Alt + .`: go forward by `zoom` seconds
- `[`: zoom in
- `]`: zoom out
- `Ctrl + s`: save as PNG
- `Ctrl + h`: show keyboard shortcuts
- `Ctrl + q`: close

For epoched signal:
```julia
ispectrogram_ep(e10, ch="Fp1")
```

![](images/ispectrogram_ep.png)

Keyboard shortcuts:

- `Home`: go to the first epoch
- `End`: go to the last epoch
- `Ctrl + ,`: previous epoch
- `Ctrl + .`: next epoch
- `[`: zoom in
- `]`: zoom out
- `Ctrl + s`: save as PNG
- `Ctrl + h`: show keyboard shortcuts
- `Ctrl + q`: close

For continuous wavelet transformation, custom wavelet formula may be entered in the field (default is `Morlet(2π), β=32, Q=128`). Please refer to the documentation of [DSP.jl](https://dsweber2.github.io/ContinuousWavelets.jl/dev/coreType/#Available-Wavelet-Families) to see the list of available wavelet families and parameters.

(!) Shift + mouse wheel scrolls the signal by one second.

(!) Ctrl + mouse wheel scrolls the signal by one segment (default is 10 seconds).

## itopo()

To preview interactively topographical plots, use:
```julia
itopo(eeg, ch="eeg")
```

![](images/itopo.png)

Available shortcuts:

Keyboard shortcuts:

- `Ctrl + s`: save as PNG
- `Ctrl + h`: show keyboard shortcuts
- `Ctrl + q`: close

(!) Segment defines time in seconds.

Epoched signal:
```julia
itopo_epoch(e10, ch="eeg")
```

![](images/itopo_ep.png)

MEG:
```julia
itopo(meg, ch="mag")
```

![](images/itopo_meg.png)
