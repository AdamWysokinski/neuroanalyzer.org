---
title: NeuroAnalyzer roadmap
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer roadmap

Number of `!` characters indicate priority. Items that are still considered are marked with `?` character.

## General

- `!` add general GUI for importing/processing/analyzing (`nalab()`)
- `!!!` support for NeuroAnalyzer.STUDY object
- `?` separate `plot_*()` into `plot_*_cont()` and `plot_epoch_*()`
- `!` update tutorials
- `!` better documentation
- `!!` better code comments
- `?` replace Gtk.jl with Gtk4.jl
- `!` more performance optimizations
- `!` distributed computing for pipelines
- `!!` improve support for MEG data
- `!` improve support for ECoG data
- `!` improve support for NIRS data
- `!` support for IEEG data
- `!` support for SEEG data
- `!` use eyetracker to detect eye movement and blink artifacts
- `?` use 3D scanner for head mesh, e.g. [Polhemus](https://polhemus.com/scanning-digitizing/fastscan)
- `?` use 3D tracker for electrode locations, e.g. [Polhemus](https://polhemus.com/motion-tracking/all-trackers/g4)
- `?` organize data using [BIDS](https://github.com/bids-standard/bids-specification) standard -- should this be internal or external?
- `!` NeuroStim: TES modeling
- `!` NeuroStim: removal of TES artifacts from EEG
- `?` NeuroStim: transcranial focused ultrasound (tFUS)
- `?` NeuroStim: transcranial infrared laser stimulation (TILS) 
- `!` NeuroTester: visual / auditory stimuli presentation module (via Raspberry Pi)
- `?` AMD ROCm / Intel Arc / Apple Metal acceleration
- `!` NIRS: add channels locations in the middle of optodes
- `!` NIRS: head plot in topoplot
- `!` more interpolation methods

## IO

- `!` better auto-detection of file format
- `!` more import formats
- `!` NWB:
    - check .TSV naming scheme
    - check .JSON naming scheme
    - check how mixed (EEG/IEEG/SEEG) recording is handled
    - check STIMULI vs EVENTS

## Edit

- `!` automated channel rejection
- `!` automated epoch rejection
- `!` insert channel

## Process

- `!!!` `reference_custom()` should calculate position of channels and add them to locs
- `!` automated cleaning of artifacts
- `!` [REST referencing](https://github.com/sccn/REST)
- `!` multitaper: generate frequency-band-selective tapers to increase sensitivity, varying the length of time segments, varying the number of tapers and central frequency of the spectral representation of the tapers

## Analysis

- `!!` add more estimators to `entropy()` and `mutual_information()` (see https://github.com/Tchanders/InformationMeasures.jl)
- `!` `ierp()` -- based on ERPLAB
- `!` wavelet-based coherence
- `?` reports in .md format (use Pandoc to convert to HTML/ODT/DOCX/PDF)
- `!` amplitude turbulence
- `?` ANOVA test for segments
- `!` CDR: current density reconstruction (GCDR, CDR spectrum), activity within specified band
- `!` frequency bands: medial vs left vs right channels within each band
- `?` compare components
- `?` correlations between components
- `!` source localization
    - dipoles
    - beamforming, leakage correction
- `!` FOOOF
- `!` import and process MRI data for EEG source localization
- `!!` connectivity:
    - neural Granger causality (NGC)
    - phase synchronization measurements: weighted PLI, phase coherence (PC), imaginary component of coherency (IC)
    - cross-frequency phase-amplitude coupling
    - phase-amplitude cross-frequency coupling (PAC)
    - power envelope connectivity
- `!` probability maps: the local likelihood of belonging to a given population
- `!` signals/PSD comparison
- `!` tensor and other statistical maps (magnitude and direction, probabilistic regions, regions of high vs low variability)
- `!` support for clusterDepth.jl
- `!` support for Unfold.jl/UnfoldMakie.jl

## Plots

- `!!!` fix: do not plot deleted NIRS channels
- `!` click on the component to select it in `iplot_icatopo()`
- `!` click on the PSD to enlarge it in `plot_psd(type=:topo)` 
- `!` brain/head mesh in `plot_locs3d()`
- `!!` improve `plot_locs()` for NIRS
- `!` brain topography
- `!` connectome graph
- `!` 3d head/brain surface plots
- `!` coherence spectrum (y: relative amplitude, x: frequencies)
- `!` ITPC topoplot
- `!` plot OBJ + component
