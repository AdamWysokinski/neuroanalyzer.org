---
title: NeuroAnalyzer plugins
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer glossary

The glossary provides definitions of terms specific for NeuroAnalyzer.

`channel`: single vector of values, usually recorded over time. Channel has a label (e.g. "Fp1"), its type (e.g. "eeg" or "eog"), a unit (e.g. "μV"). Available NeuroAnalyzer channel types are stored in the `NeuroAnalyzer.channel_types` variable.

`component`: is data produced by processing or analyzing neurophysiological data, e.g. primary components. They can be stored together with the parent data within the NEURO object and accessed for further analysis.

`data`: is a vector (e.g. single channel), a matrix (e.g. channels × channels or channels × epochs) or an array (channels × time × epoch) of a neurophysiological recording data. Available NeuroAnalyzer data types are:

- `eeg`: electroencephalography (EEG)
- `ieeg`: intracranial electroencephalography (iEEG):
    - `ecog`: electrocorticography (ECoG)
    - `seeg`: stereoelectroencephalography (SEEG)
- `cr`: cellular recording, such as intracellular and extracellular recordings
- `meg`: magnetoencephalography (MEG)
- `nirs`: near-infrared spectroscopy (NIRS)
- `sensors`: various body sensors
- `eda`: electrodermal activity
- `mep`: motor evoked potentials

`epoch`: segment of recorded data; epochs may be defined using markers (e.g. stimuli / events) or time intervals. Every recording data imported to NeuroAnalyzer has at least one epoch.

`label`: name attributed to a channel, e.g. "Fp1" for EEG or "S1_D1 OD" for NIRS.

`locs`: positions of electrodes / detectors used to record neurophysiological data (e.g. EEG electrodes or NIRS optodes). Positions are defined in 2D and 3D space separately using Cartesian (X, Y, Z), planar (radius and theta) and spherical (radius, theta and phi) coordinates. NeuroAnalyzer uses RAS (Right-Anterior-Superior) coordination system: `R`: +X is right, -X is left, `A`: +Y is anterior (front), -Y is posterior (rear), `S`: +Z is superior (top), -Z is inferior (bottom).

`marker`: event that has been recorded simultaneously with neurophysiological data, e.g. push of a button. Each marker has an id (e.g. `S01`), onset (in samples), duration (in samples) and description (e.g. `stim`).

`mutator`: function that modifies the input object instead of returning it. Mutator functions are depicted using `!` character in their names, e.g. `delete_channel!()` or `add_locs!()`.

`NEURO object`: the fundamental object of neurophysiological data on which NeuroAnalyzer operates. Each object contains a header (with recording, subject and experiment meta-data), time points, epoch time points, recording data, channel locations, event markers and processing history. A new object is created by importing a neurophysiological recording, e.g. EEG or MEG.

`pick`: defines a set of channel locations, e.g. [:left, :frontal] pick would be ["Fp1", "F3", "F7"] channels.

`signal channel`: channels that contain neurophysiological recorded data, e.g. for EEG these would be "Fp1" or "C3" labeled channels, for MEG data these would be magnetometers ("meg") and gradiometers ("grad") type channels. For EEG/SEEG/ECOG or MEG recordings this would also include reference (e.g. "A1") and oculogram ("EOG1") channels.
