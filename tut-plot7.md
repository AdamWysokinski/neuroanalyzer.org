---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: auto/cross-correlation/covariance

Plot autocovariance:
```julia
ac, l = acov(e10, l=100)
p = plot_xac(ac[1, :, 1], l, title="Autocovariance")
plot_save(p, file_name="images/acov.png")
```
![](images/acov.png)

There are repetitive positive peaks every 0.2 s, due to dominating 50 Hz line noise in the source signal. Every 0.1 s there are negative peaks due to shifting the signal by 1/2 of the 50 Hz cycle. `axc2frq()` can be used to detect these peaks and transform into frequencies:

```julia
ac, l = acor(eeg, l=10)
axc2frq(ac[1, :, 1], l)
```

Output:

    1-element Vector{Float64}:
     50.0

By default, biased autocovariance is calculated. To calculate unbiased autocovariance:
```julia
ac, l = acov(e10, biased=false)
```

Plot autocorrelation (which is a standardized covariance):
```julia
ac, l = acor(e10, l=100)
p = plot_xac(ac[1, :, 1], l, title="Autocorrelation")
plot_save(p, file_name="images/acor.png")
```
![](images/acor.png)

Plot cross-covariance:
```julia
xc, l = xcov(e10, e10, ch1=1, ch2=2, l=100)
p = NeuroAnalyzer.plot_xac(xc[1, :, 1], l, title="Cross-covariance")
plot_save(p, file_name="images/xcov.png")
```
![](images/xcov.png)

Plot cross-correlation (which is a standardized cross-covariance):
```julia
xc, l = xcor(e10, e10, ch1=1, ch2=2, l=100)
p = NeuroAnalyzer.plot_xac(xc[1, :, 1], l, title="Cross-correlation")
plot_save(p, file_name="images/xcor.png")
```
![](images/xcor.png)