---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Analyze EEG (5)

Lateralization index is calculated as log(A / B), where A is average power at given frequency (default is 10 Hz, alpha) for the right hemisphere and B is average power at that frequency for the left hemisphere.

Lateralization index (default for alpha 10 Hz frequency):
```julia
lidx = lat_idx(eeg)
```

Output:

    1.0831759729927761

Lateralization index for the range 8.0-12.5 Hz:
```julia
lidx = lat_idx(eeg, frq=(8.0, 12.5))
```

Output:

    1.0362690736517686

Calculate frontal alpha asymmetry index:
```julia
# left
ch_left = channel_cluster(eeg, cluster=:f1)
# or
ch_left = channel_pick(eeg, p=[:left, :frontal])
# right
ch_right = channel_cluster(eeg, cluster=:f2)
# or
ch_right = channel_pick(eeg, p=[:right, :frontal])

# alpha frequency
alpha = band_frq(eeg, band=:alpha)

# asymmetry index
band_asymmetry(eeg, frq_lim=alpha, ch1=ch_left, ch2=ch_right)
```
