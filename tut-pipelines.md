---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: EEG pipelines

Pipelines are stored in `~/NeuroAnalyzer/pipelines`.

```julia
include("$(homedir())/NeuroAnalyzer/pipelines/test_pipeline.jl")
eeg2 = test_pipeline(eeg)
```