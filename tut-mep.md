---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: MEPs

Motor evoked potentials (MEPs) are the electrical signals recorded from the descending motor pathways or from muscles following stimulation of motor pathways within the brain (source: [Encyclopedia of the Neurological Sciences](https://www.sciencedirect.com/science/article/abs/pii/B978012385157401109X)).

MEPs are used in Transcranial Magnetic Stimulation (TMS) for determining the intensity of stimulation.

NeuroAnalyzer allows analysis of MEP recordings. Usually recording contains more than one MEP. Each MEP is imported as individual channel of the NeuroAnalyzer.NEURO object.

To load MEP recording exported from DuoMAG stimulator:
```julia
mep = import_duomag("mep-duomag.m")
# or
mep = import_duomag("mep-duomag.ascii")
```

Show recording details:
```julia
info(mep)
```

                  Data type: MEP
                File format: DuoMAG
                Source file: mep-duomag.m
             File size [MB]: 0
           Memory size [MB]: 1.25
                    Subject: 000011:  5-1 
                  Recording: 
            Recording notes: 
             Recording date: 21.11.2019
             Recording time: 21.11.2019
         Sampling rate (Hz): 12500
    Signal length [samples]: 8129
    Signal length [seconds]: 0.65
         Number of channels: 19
                     Labels: yes
                 Components: no
    Channels:
     ch     label           type        unit    
     ------ --------------- ----------- -------
     1      MEP1            MEP         μV      
     2      MEP2            MEP         μV      
     3      MEP3            MEP         μV      
     4      MEP4            MEP         μV      
     5      MEP5            MEP         μV      
     6      MEP6            MEP         μV      
     7      MEP7            MEP         μV      
     8      MEP8            MEP         μV      
     9      MEP9            MEP         μV      
     10     MEP10           MEP         μV      
     11     MEP11           MEP         μV      
     12     MEP12           MEP         μV      
     13     MEP13           MEP         μV      
     14     MEP14           MEP         μV      
     15     MEP15           MEP         μV      
     16     MEP16           MEP         μV      
     17     MEP17           MEP         μV      
     18     MEP18           MEP         μV      
     19     MEP19           MEP         μV

Detect peaks:
```julia
mep_peaks(mep)
```

(!) The output matrix contains the position (in samples) of the positive (first column) and negative (second column) peaks.

Plot MEP:
```julia
p = plot_mep(mep, ch=1)
plot_save(p, file_name="images/mep1.png")
```

Output:

    [ Info: Positive peak time: 28.0 ms
    [ Info: Positive peak amplitude: 65.69 μV
    [ Info: Negative peak time: 26.0 ms
    [ Info: Negative peak amplitude: -95.31 μV

(!) These peaks are auto-detected. Files exported from DuoMAG contain peaks detected by the stimulator. To use embedded peaks, use:
```julia
p = plot_mep(mep, ch=1, peaks_detect=false)
```

![](images/mep1.png)

Butterfly plot over MEPs:
```julia
p = plot_mep(mep, ch=1:10, type=:butterfly, avg=true)
plot_save(p, file_name="images/mep2.png")
```

![](images/mep2.png)

Mean plot over MEPs:
```julia
p = plot_mep(mep, ch=1:19, type=:mean, peaks=false)
plot_save(p, file_name="images/mep3.png")
```

![](images/mep3.png)

Plot stacked MEPs:
```julia
p = plot_mep(mep, ch=1:19, type=:stack)
plot_save(p, file_name=file_name="images/mep4.png")
```

![](images/mep4.png)

Calculate amplitude at given time point across all MEPs:
```julia
amp_at(mep, t=0.250)
```

Calculate average, minimum and maximum amplitude over a time segment:
```julia
avgamp_at(mep, t=(0.150, 0.250))
minamp_at(mep, t=(0.150, 0.250))
maxamp_at(mep, t=(0.150, 0.250))
```
