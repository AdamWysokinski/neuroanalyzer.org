---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Analyze EEG (6)

Calculate epochs statistics (mean, median, standard deviation, variance, kurtosis, skewness, mean diff value, median diff value, max difference, deviation from channel mean):
```julia
epoch_stats(eeg)
```

Calculate channels statistics (mean, median, standard deviation, variance, kurtosis, skewness, mean diff value, median diff value, max difference, deviation from channel mean) per epoch:
```julia
channel_stats(eeg)
```

Calculate covariance matrix:
```julia
covm(eeg)
```

Calculate correlation matrix
```julia
corm(eeg)
```

Calculate auto-covariance:
```julia
acov(eeg, lag=20, norm=false)
```

Calculate cross-covariance:
```julia
cc, lags = xcov(eeg, lag=20, demean=true)

eeg1 = filter(eeg, fprototype=:butterworth, ftype=:bs, cutoff=(45, 55), order=8)
eeg2 = filter(eeg, fprototype=:butterworth, ftype=:bs, cutoff=(45, 55), order=12)
cc, lags = xcov(eeg1, eeg2, ch1="F3", ch2="F4", ep1=1, ep2=1, lag=20, demean=true, norm=true)
```

Mutual information:
```julia
m = mutual_information(eeg, ch=get_channel(eeg, type="eeg"))
plot_matrix(m[:, :, 1], xlabels=labels(eeg)[1:19], ylabels=labels(eeg)[1:19])
```

(!) Currently only one estimator (maximum likelihood) is available. Internally it uses `InformationMeasures.get_mutual_information()`, source: [InformationMeasures.jl](https://github.com/Tchanders/InformationMeasures.jl).

Entropy:
```julia
e, _, _ = entropy(eeg, ch=get_channel(eeg, type="eeg"))
plot_bar(e[:, 1], labels=labels(eeg)[1:19], seriestype=:bar, title="Entropy, epoch 1")
```

(!) Entropy of the signal is calculated using its histogram bins (number of bins is calculated using the Freedman-Diaconis formula) using the formulas `p = n / sum(n)` and `entropy = -sum(p .* log2(p))`, where `p` is the probability of each bin and `n` are bins' weights. Shannon entropy and log energy entropy are calculated using `Wavelets.coefentropy()`.

Calculate ISPC (Inter-Site-Phase Clustering):
```julia
ispc(eeg, eeg, ch1=get_channel(eeg, type="eeg")[1:5], ch2=get_channel(eeg, type="eeg")[6:10], ep1=1, ep2=1)
```

Calculate PLI (Phase Lag Index):
```julia
pli(eeg, eeg, ch1="F3", ch2="F4", ep1=1, ep2=1)
```

Amplitude Envelope Correlation:
```julia
env_cor(eeg, eeg, ch1="F3", ch2="F4", ep1=1, ep2=2)
```

Perform discrete wavelet transform (DWT) and continuous wavelet transform (CWT):
```julia
dw_trans(e10, wt=wavelet(WT.haar), type=:sdwt)
cw_trans(e10, wt=wavelet(Morlet(π), β=2))
```

SNR (signal-to-noise) may be estimated from epoched EEG signal. 
```julia
snr, hz = snr(eeg)
p = plot(hz, snr[1, :], label="ch 1", xlabel="Frequency [Hz]", ylabel="SNR")
p = plot!(hz, snr[2, :], label="ch 2")
p = plot!(hz, snr[3, :], label="ch 3")
p = plot!(hz, snr[4, :], label="ch 4")
plot_save(p, file_name="images/eeg_snr.png")
```

![](images/eeg_snr.png)
