---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Plot band powers

Plot band power in the δ, θ, α and β ranges:
```julia
using DSP
using Plots

# generate frequencies
bands = [:delta, :theta, :alpha, :beta]
frq = Vector{Tuple{Float64, Float64}}()
for idx in bands
    push!(frq, band_frq(eeg, band=idx))
end

# calculate power for channel 2, epoch 1
bp = Vector{Float64}()
ch = 2
ep = 1
for idx in frq
    push!(bp, band_power(eeg, frq_lim=idx)[ch, ep])
end

# plot PSD
p = plot_psd(eeg, ch=1, ep=1, db=false, frq_lim=(0, 30), mono=true)
# draw band ranges
vline!(p, frq, lc=:black, alpha=0.5, lw=0.5)
plot_save(p, file_name="images/eeg_psd_0-30.png")

# plot band powers
p = plot_bar(bp, labels=string.(bands), xlabel="", ylabel="Power [μV²/Hz]", title="Band powers\n[channel: $ch, epoch: $ep]")
plot_save(p, file_name="images/eeg_bp.png")
```

![](images/eeg_psd_0-30.png)
![](images/eeg_bp.png)

Plot PSD relative to alpha band power:
```julia
p = plot_psd(eeg, ep=ep, ch=ch, ref=:alpha, frq_lim=(0, 30), mono=true)
vline!(p, frq, lc=:black, alpha=0.5, lw=0.5)
plot_save(p, file_name="images/eeg_psd_rel_alpha.png")
```
![](images/eeg_psd_rel_alpha.png)
