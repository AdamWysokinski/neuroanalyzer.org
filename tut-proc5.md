---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Process EEG (5)

## ICA deconstruct

To deconstruct all EEG channels into defined number of components:
```julia
ic, ic_mw, ic_var = ica_decompose(eeg, ch="eeg", n=3)
```

Output:

    [ Info: Attempting to calculate 3 components
    [ Info: Training will end when W change = 0.99 or after 900 steps
    [ Info: Data will be demeaned and pre-whitened
    [ Info: Converged at: 1.0e-6
    [ Info: Component  1: percent variance accounted for: 67.59
    [ Info: Component  2: percent variance accounted for: 23.4
    [ Info: Component  3: percent variance accounted for: 4.59

(!) Components are sorted in the order of decreasing variance accounted for (the higher the value, the higher the component accounts for all the data).

(!) By default, 100 iterations per each tolerance value (`[0.000001, 0.00001, 0.0001, 0.001, 0.01, 0.1, 0.5, 0.9, 0.99]`); hence the default 100 iterations gives 900 steps.

ICA components and weights may be included in the NeroAnalyzer object as embedded components:
```julia
add_component!(eeg, c=:ic, v=ic)
add_component!(eeg, c=:ic_mw, v=ic_mw)
```

(!) Embedded components are removed from the object when the signal data is altered in any way.

(!) Embedded components are saved with the signal data when using `save()`.

## ICA reconstruct

To reconstruct the source signal without a specific component(s):
```julia
eeg_new = ica_reconstruct(eeg, ic, ic_mw, ch="eeg", ic_idx=[1, 3])
iview(eeg, eeg_new)
```

(!) By default `ic_idx` is the list of components that should be **removed** during reconstruction.

To reconstruct the source signal keeping **only** the selected component(s), use `keep=true` option:
```julia
eeg_new = ica_reconstruct(eeg, ic, ic_mw, ch="eeg", ic_idx=1:2, keep=true)
iview(eeg, eeg_new)
```

To use the embedded component for reconstruction:
```julia
ica_reconstruct!(eeg, ic_idx=1)
```

(!) The components must be named `:ic` and `:ic_mw`.

To remove the component from the signal:
```julia
ica_remove(eeg, ic, ic_mw, ch="eeg", ic_idx=1)
```

(!) `ica_remove()` reconstructs the source signal using only the specified component(s) and subtract the reconstructed signal from the source.

## ICA: plotting

To plot the components:
```julia
p1 = NeuroAnalyzer.plot(eeg, ch="eeg", mono=true, seg=(0, 20), title="EEG channels")
p2 = NeuroAnalyzer.plot(eeg, ic, mono=true, seg=(0, 20), title="ICA components")
p = Plots.plot(p1, p2, layout=(2, 1))
plot_save(p, file_name="images/ica_components.png")
```

![](images/ica_components.png)

To plot the components power spectrum:
```julia
p1 = plot_psd(eeg, ch="Fp1", title="original signal", mono=true)
p2 = plot_psd(eeg, ic, c_idx=2, title="ICA #02", mono=true)
eeg_new = ica_reconstruct(eeg, ic, ic_mw, ch="eeg", ic_idx=2, keep=true)
p3 = plot_psd(eeg_new, ch="Fp1", title="signal reconstructed from ICA #02", mono=true)
eeg_new = ica_reconstruct(eeg, ic, ic_mw, ch="eeg", ic_idx=2)
p4 = plot_psd(eeg_new, ch="Fp1", title="signal after removing ICA #02", mono=true)
p = Plots.plot(p1, p2, p3, p4, layout=(2, 2))
plot_save(p, file_name="images/ica_components_psd.png")
```
![](images/ica_components_psd.png)

To plot the component spectrogram:
```julia
p1 = plot_spectrogram(eeg, seg=(0, 10), ch="Fp1", title="original signal")
p2 = plot_spectrogram(eeg, ic, c_idx=2, title="ICA #02")
eeg_new = ica_reconstruct(eeg, ic, ic_mw, ch="eeg", ic_idx=2, keep=true);
p3 = plot_spectrogram(eeg_new, ch="Fp1", title="signal reconstructed from ICA #02")
eeg_new = ica_reconstruct(eeg, ic, ic_mw, ch="eeg", ic_idx=2);
p4 = plot_spectrogram(eeg_new, ch="Fp1", title="signal after removing ICA #02")
p = Plots.plot(p1, p2, p3, p4, layout=(2, 2))
plot_save(p, file_name="images/ica_components_spec.png")
```

![](images/ica_components_spec.png)

(!) `ch` is the channel number; `ic_idx` is the component number.

Preview and process ICA interactively:
```julia
iview_ica(eeg, ic, ic_mw, ch="eeg")
```

or when using embedded components:
```julia
iview_ica(eeg, ch="eeg")
```

![](images/iview_ica.png)

(!) The keyboard shortcut `ctrl-q` closes the window.

Topographical maps of all ICA components are shown in the left part of the window. Top plot shows PSD of the signal or IC component. Bottom plot shows amplitude of the signal or IC component.

The difference between reconstruct and remove methods is that the first method reconstructs the signal from the given component. In the remove method the given component is removed from the original signal.

Mark components you would like to remove by clicking the `Mark` button. After clicking the `Reconstruct` or `Remove` button, all components marked for removal will be removed from the signal.

When you are satisfied with the changes, press `Apply` to apply all alterations to the original object and close this window. Pressing `Cancel` to abandon all changes (the original object will not be affected) and close the window.

## Removing ECG artifacts using ICA

EEG has already been filtered at 1 Hz and 40 hz:
```julia
ic, ic_mw, ic_var = ica_decompose(eeg, ch=["eeg", "ref", "eog", "ecg"])
eeg_new = ica_reconstruct(eeg, ic, ic_mw, ch=["eeg", "ref", "eog"], ic_idx=2, keep=false)
iview(eeg, eeg_new)
```

Output:

    [ Info: Converged at: 1.0e-6
    [ Info: Component  1: percent variance accounted for: 41.74
    [ Info: Component  2: percent variance accounted for: 29.21
    [ Info: Component  3: percent variance accounted for: 19.36
    [ Info: Component  4: percent variance accounted for: 3.03

![](images/ecg_ica.png)