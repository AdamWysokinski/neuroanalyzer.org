---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Analyze EEG (4)

Coherence and magnitude-squared coherence:
```julia
# use multi-taper method
coh, msc, f = coherence(eeg, eeg, method=:mt)
# use FFT method
coh, msc, f = coherence(eeg, eeg, method=:fft)

eeg_alpha = NeuroAnalyzer.filter(eeg, fprototype=:butterworth, ftype=:bp, cutoff=band_frq(eeg, band=:alpha), order=8)
# coherence between O1 vs O2 in the alpha band
coh, msc, f = coherence(eeg_alpha, eeg_alpha, ch1="O1", ch2="O2", ep1=1, ep2=1, frq_lim=band_frq(eeg, band=:alpha))
p = Plots.plot(f, coh[1, :, 1], label="coherence", xlabel="Frequency [Hz]")
p = Plots.plot!(f, msc[1, :, 1], label="MSC")
plot_save(p, file_name="images/coh1.png")
```
![](images/coh1.png)

Also, `plot_coherence()` function is available:
```julia
coh, mscoh, f = coherence(eeg, eeg, ch1=get_channel(eeg, type="eeg")[1:4], ch2=get_channel(eeg, type="eeg")[5:8], ep1=1, ep2=1, frq_lim=band_frq(eeg, band=:alpha))
# simplify values
coh, _ = areduce(coh, f, n=0.1)
mscoh, f = areduce(mscoh, f, n=0.1)
# generate label pairs
l = paired_labels(get_channel(eeg, type="eeg")[1:4], get_channel(eeg, type="eeg")[5:8])
# plot coherence values along frequencies
p = plot_coherence(coh[1, :, 1], f, title="Coherence $(l[1])")
plot_save(p, file_name="images/coh2.png")
```

![](images/coh2.png)

Output:

    [ Info: Minimum coherence 0.253 at 12.3 Hz
    [ Info: Maximum coherence 0.854 at 11.6 Hz

For multi-channel data:
```julia
p = plot_coherence(mscoh[:, :, 1], f, clabels=l, title="Magnitude-squared coherence")
plot_save(p, file_name="images/coh3.png")
p = plot_coherence_avg(mscoh[:, :, 1], f, clabels=l, title="Averaged magnitude-squared coherence")
plot_save(p, file_name="images/coh4.png")
p = plot_coherence_butterfly(mscoh[:, :, 1], f, clabels=l, title="Magnitude-squared coherence")
plot_save(p, file_name="images/coh5.png")
```

Output:

    [ Info: Channel pair Fp1-C3 minimum coherence 0.064 at 12.3 Hz
    [ Info: Channel pair Fp1-C3 maximum coherence 0.729 at 11.6 Hz
    [ Info: Channel pair Fp2-C4 minimum coherence 0.002 at 9.2 Hz
    [ Info: Channel pair Fp2-C4 maximum coherence 0.785 at 8.4 Hz
    [ Info: Channel pair F3-P3 minimum coherence 0.059 at 11.2 Hz
    [ Info: Channel pair F3-P3 maximum coherence 0.924 at 12.9 Hz
    [ Info: Channel pair F4-P4 minimum coherence 0.003 at 10.4 Hz
    [ Info: Channel pair F4-P4 maximum coherence 0.525 at 10.3 Hz

![](images/coh3.png)
![](images/coh4.png)
![](images/coh5.png)

Plot averaged coherence values as connections:
```julia
load_locs!(eeg, file_name=joinpath(NeuroAnalyzer.PATH, "locs", "standard-10-20-cap19-elmiko.ced"))
c = zeros(19, 19)
coh_avg = round.(mean(coh[:, :, 1], dims=2), digits=2)
c[1, 5] = coh_avg[1]
c[2, 6] = coh_avg[2]
c[3, 7] = coh_avg[3]
c[4, 8] = coh_avg[4]
p = plot_connections(eeg, connections=c)
plot_save(p, file_name="images/coh6.png")
```

![](images/coh6.png)

(!) Line thickness represents connectivity value. If `mono=true`, positive values are represented with gray lines, negative -- with dotted gray lines. If `mono=false`, positive values are represented with red lines, negative -- with blue lines.

To presents values numerically, set the option `weights` to false. Positive values

```julia
p = plot_connections(eeg, connections=c, weights=false)
plot_save(p, file_name="images/coh7.png")
```

![](images/coh7.png)

(!) If `mono=false`, positive values are in red and negative values are in blue.