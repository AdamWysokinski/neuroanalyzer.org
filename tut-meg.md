---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: MEG

Currently, MEG functionality is very limited.

Load MEG data:
```julia
using MAT
file_name = "meg-raw.fiff"
meg = import_fiff(file_name)
```

Show signal details:
```julia
info(meg)
```

Output:

                  Data type: MEG
                File format: FIFF
                Source file: meg-raw.fiff
             File size [MB]: 40.0
           Memory size [MB]: 80.38
                    Subject: 2: Subject Subject
                  Recording: TRIUX system
            Recording notes: 
             Recording date: 
             Recording time: 
         Sampling rate (Hz): 1000
    Signal length [samples]: 32000
    Signal length [seconds]: 32.0
         Number of channels: 327
                  Epochs ID: 
           Number of epochs: 1
     Epoch length [samples]: 32000
     Epoch length [seconds]: 32.0
            SSP projections: PCA-v1, PCA-v2, PCA-v3
             Line frequency: 50 Hz
                     Labels: yes
                    Markers: no
          Channel locations: yes
                 Components: no
     Number of MAG channels: 102
    Number of GRAD channels: 204
     Number of EEG channels: 60
    Channels:
     ch     label           type        unit    bad     
     ------ --------------- ----------- ------- -------
     1      IAS 307         IAS         V       false   
     2      IAS 306         IAS         V       false   
     3      IAS 309         IAS         V       false   
     4      IAS 308         IAS         V       false   
     5      IAS 311         IAS         V       false   
     6      IAS 310         IAS         V       false   
     7      IAS 302         IAS         V       false   
     8      IAS 304         IAS         V       false   
     9      IAS 301         IAS         V       false   
     10     IAS 303         IAS         V       false   
     11     IAS 305         IAS         V       false   
     12     MEG 111         MAG         fT      false   
     13     MEG 112         GRAD        fT/cm   false   
     14     MEG 113         GRAD        fT/cm   false   
     15     MEG 121         MAG         fT      false   
     16     MEG 122         GRAD        fT/cm   false   
     17     MEG 123         GRAD        fT/cm   false   
     18     MEG 131         MAG         fT      false   
     19     MEG 132         GRAD        fT/cm   false   
     20     MEG 133         GRAD        fT/cm   false   
     21     MEG 141         MAG         fT      false   
     22     MEG 142         GRAD        fT/cm   false   
     23     MEG 143         GRAD        fT/cm   false   
     24     MEG 211         MAG         fT      false   
     25     MEG 212         GRAD        fT/cm   false   
     26     MEG 213         GRAD        fT/cm   false   
     27     MEG 221         MAG         fT      false   
     28     MEG 222         GRAD        fT/cm   false   
     29     MEG 223         GRAD        fT/cm   false   
     30     MEG 231         MAG         fT      false   
     31     MEG 232         GRAD        fT/cm   false   
     32     MEG 233         GRAD        fT/cm   false   
     33     MEG 241         MAG         fT      false   
     34     MEG 242         GRAD        fT/cm   false   
     35     MEG 243         GRAD        fT/cm   false   
     36     MEG 311         MAG         fT      false   
     37     MEG 312         GRAD        fT/cm   false   
     38     MEG 313         GRAD        fT/cm   false   
     39     MEG 321         MAG         fT      false   
     40     MEG 322         GRAD        fT/cm   false   
     41     MEG 323         GRAD        fT/cm   false   
     42     MEG 331         MAG         fT      false   
     43     MEG 332         GRAD        fT/cm   false   
     44     MEG 333         GRAD        fT/cm   false   
     45     MEG 341         MAG         fT      false   
     46     MEG 342         GRAD        fT/cm   false   
     47     MEG 343         GRAD        fT/cm   false   
     48     MEG 411         MAG         fT      false   
     49     MEG 412         GRAD        fT/cm   false   
     50     MEG 413         GRAD        fT/cm   false   
     51     MEG 421         MAG         fT      false   
     52     MEG 422         GRAD        fT/cm   false   
     53     MEG 423         GRAD        fT/cm   false   
     54     MEG 431         MAG         fT      false   
     55     MEG 432         GRAD        fT/cm   false   
     56     MEG 433         GRAD        fT/cm   false   
     57     MEG 441         MAG         fT      false   
     58     MEG 442         GRAD        fT/cm   false   
     59     MEG 443         GRAD        fT/cm   false   
     60     MEG 511         MAG         fT      false   
     61     MEG 512         GRAD        fT/cm   false   
     62     MEG 513         GRAD        fT/cm   false   
     63     MEG 521         MAG         fT      false   
     64     MEG 522         GRAD        fT/cm   false   
     65     MEG 523         GRAD        fT/cm   false   
     66     MEG 531         MAG         fT      false   
     67     MEG 532         GRAD        fT/cm   false   
     68     MEG 533         GRAD        fT/cm   false   
     69     MEG 541         MAG         fT      false   
     70     MEG 542         GRAD        fT/cm   false   
     71     MEG 543         GRAD        fT/cm   false   
     72     MEG 611         MAG         fT      false   
     73     MEG 612         GRAD        fT/cm   false   
     74     MEG 613         GRAD        fT/cm   false   
     75     MEG 621         MAG         fT      false   
     76     MEG 622         GRAD        fT/cm   false   
     77     MEG 623         GRAD        fT/cm   false   
     78     MEG 631         MAG         fT      false   
     79     MEG 632         GRAD        fT/cm   false   
     80     MEG 633         GRAD        fT/cm   false   
     81     MEG 641         MAG         fT      false   
     82     MEG 642         GRAD        fT/cm   false   
     83     MEG 643         GRAD        fT/cm   false   
     84     MEG 711         MAG         fT      false   
     85     MEG 712         GRAD        fT/cm   false   
     86     MEG 713         GRAD        fT/cm   false   
     87     MEG 721         MAG         fT      false   
     88     MEG 722         GRAD        fT/cm   false   
     89     MEG 723         GRAD        fT/cm   false   
     90     MEG 731         MAG         fT      false   
     91     MEG 732         GRAD        fT/cm   false   
     92     MEG 733         GRAD        fT/cm   false   
     93     MEG 741         MAG         fT      false   
     94     MEG 742         GRAD        fT/cm   false   
     95     MEG 743         GRAD        fT/cm   false   
     96     MEG 811         MAG         fT      false   
     97     MEG 812         GRAD        fT/cm   false   
     98     MEG 813         GRAD        fT/cm   false   
     99     MEG 821         MAG         fT      false   
     100    MEG 822         GRAD        fT/cm   false   
     101    MEG 823         GRAD        fT/cm   false   
     102    MEG 911         MAG         fT      false   
     103    MEG 912         GRAD        fT/cm   false   
     104    MEG 913         GRAD        fT/cm   false   
     105    MEG 921         MAG         fT      false   
     106    MEG 922         GRAD        fT/cm   false   
     107    MEG 923         GRAD        fT/cm   false   
     108    MEG 931         MAG         fT      false   
     109    MEG 932         GRAD        fT/cm   false   
     110    MEG 933         GRAD        fT/cm   false   
     111    MEG 941         MAG         fT      false   
     112    MEG 942         GRAD        fT/cm   false   
     113    MEG 943         GRAD        fT/cm   false   
     114    MEG 1011        MAG         fT      false   
     115    MEG 1012        GRAD        fT/cm   false   
     116    MEG 1013        GRAD        fT/cm   false   
     117    MEG 1021        MAG         fT      false   
     118    MEG 1022        GRAD        fT/cm   false   
     119    MEG 1023        GRAD        fT/cm   false   
     120    MEG 1031        MAG         fT      false   
     121    MEG 1032        GRAD        fT/cm   false   
     122    MEG 1033        GRAD        fT/cm   false   
     123    MEG 1041        MAG         fT      false   
     124    MEG 1042        GRAD        fT/cm   false   
     125    MEG 1043        GRAD        fT/cm   false   
     126    MEG 1111        MAG         fT      false   
     127    MEG 1112        GRAD        fT/cm   false   
     128    MEG 1113        GRAD        fT/cm   false   
     129    MEG 1121        MAG         fT      false   
     130    MEG 1122        GRAD        fT/cm   false   
     131    MEG 1123        GRAD        fT/cm   false   
     132    MEG 1131        MAG         fT      false   
     133    MEG 1132        GRAD        fT/cm   false   
     134    MEG 1133        GRAD        fT/cm   false   
     135    MEG 1141        MAG         fT      false   
     136    MEG 1142        GRAD        fT/cm   false   
     137    MEG 1143        GRAD        fT/cm   false   
     138    MEG 1211        MAG         fT      false   
     139    MEG 1212        GRAD        fT/cm   false   
     140    MEG 1213        GRAD        fT/cm   false   
     141    MEG 1221        MAG         fT      false   
     142    MEG 1222        GRAD        fT/cm   false   
     143    MEG 1223        GRAD        fT/cm   false   
     144    MEG 1231        MAG         fT      false   
     145    MEG 1232        GRAD        fT/cm   false   
     146    MEG 1233        GRAD        fT/cm   false   
     147    MEG 1241        MAG         fT      false   
     148    MEG 1242        GRAD        fT/cm   false   
     149    MEG 1243        GRAD        fT/cm   false   
     150    MEG 1311        MAG         fT      false   
     151    MEG 1312        GRAD        fT/cm   false   
     152    MEG 1313        GRAD        fT/cm   false   
     153    MEG 1321        MAG         fT      false   
     154    MEG 1322        GRAD        fT/cm   false   
     155    MEG 1323        GRAD        fT/cm   false   
     156    MEG 1331        MAG         fT      false   
     157    MEG 1332        GRAD        fT/cm   false   
     158    MEG 1333        GRAD        fT/cm   false   
     159    MEG 1341        MAG         fT      false   
     160    MEG 1342        GRAD        fT/cm   false   
     161    MEG 1343        GRAD        fT/cm   false   
     162    MEG 1411        MAG         fT      false   
     163    MEG 1412        GRAD        fT/cm   false   
     164    MEG 1413        GRAD        fT/cm   false   
     165    MEG 1421        MAG         fT      false   
     166    MEG 1422        GRAD        fT/cm   false   
     167    MEG 1423        GRAD        fT/cm   false   
     168    MEG 1431        MAG         fT      false   
     169    MEG 1432        GRAD        fT/cm   false   
     170    MEG 1433        GRAD        fT/cm   false   
     171    MEG 1441        MAG         fT      false   
     172    MEG 1442        GRAD        fT/cm   false   
     173    MEG 1443        GRAD        fT/cm   false   
     174    MEG 1511        MAG         fT      false   
     175    MEG 1512        GRAD        fT/cm   false   
     176    MEG 1513        GRAD        fT/cm   false   
     177    MEG 1521        MAG         fT      false   
     178    MEG 1522        GRAD        fT/cm   false   
     179    MEG 1523        GRAD        fT/cm   false   
     180    MEG 1531        MAG         fT      false   
     181    MEG 1532        GRAD        fT/cm   false   
     182    MEG 1533        GRAD        fT/cm   false   
     183    MEG 1541        MAG         fT      false   
     184    MEG 1542        GRAD        fT/cm   false   
     185    MEG 1543        GRAD        fT/cm   false   
     186    MEG 1611        MAG         fT      false   
     187    MEG 1612        GRAD        fT/cm   false   
     188    MEG 1613        GRAD        fT/cm   false   
     189    MEG 1621        MAG         fT      false   
     190    MEG 1622        GRAD        fT/cm   false   
     191    MEG 1623        GRAD        fT/cm   false   
     192    MEG 1631        MAG         fT      false   
     193    MEG 1632        GRAD        fT/cm   false   
     194    MEG 1633        GRAD        fT/cm   false   
     195    MEG 1641        MAG         fT      false   
     196    MEG 1642        GRAD        fT/cm   false   
     197    MEG 1643        GRAD        fT/cm   false   
     198    MEG 1711        MAG         fT      false   
     199    MEG 1712        GRAD        fT/cm   false   
     200    MEG 1713        GRAD        fT/cm   false   
     201    MEG 1721        MAG         fT      false   
     202    MEG 1722        GRAD        fT/cm   false   
     203    MEG 1723        GRAD        fT/cm   false   
     204    MEG 1731        MAG         fT      false   
     205    MEG 1732        GRAD        fT/cm   false   
     206    MEG 1733        GRAD        fT/cm   false   
     207    MEG 1741        MAG         fT      false   
     208    MEG 1742        GRAD        fT/cm   false   
     209    MEG 1743        GRAD        fT/cm   false   
     210    MEG 1811        MAG         fT      false   
     211    MEG 1812        GRAD        fT/cm   false   
     212    MEG 1813        GRAD        fT/cm   false   
     213    MEG 1821        MAG         fT      false   
     214    MEG 1822        GRAD        fT/cm   false   
     215    MEG 1823        GRAD        fT/cm   false   
     216    MEG 1831        MAG         fT      false   
     217    MEG 1832        GRAD        fT/cm   false   
     218    MEG 1833        GRAD        fT/cm   false   
     219    MEG 1841        MAG         fT      false   
     220    MEG 1842        GRAD        fT/cm   false   
     221    MEG 1843        GRAD        fT/cm   false   
     222    MEG 1911        MAG         fT      false   
     223    MEG 1912        GRAD        fT/cm   false   
     224    MEG 1913        GRAD        fT/cm   false   
     225    MEG 1921        MAG         fT      false   
     226    MEG 1922        GRAD        fT/cm   false   
     227    MEG 1923        GRAD        fT/cm   false   
     228    MEG 1931        MAG         fT      false   
     229    MEG 1932        GRAD        fT/cm   false   
     230    MEG 1933        GRAD        fT/cm   false   
     231    MEG 1941        MAG         fT      false   
     232    MEG 1942        GRAD        fT/cm   false   
     233    MEG 1943        GRAD        fT/cm   false   
     234    MEG 2011        MAG         fT      false   
     235    MEG 2012        GRAD        fT/cm   false   
     236    MEG 2013        GRAD        fT/cm   false   
     237    MEG 2021        MAG         fT      false   
     238    MEG 2022        GRAD        fT/cm   false   
     239    MEG 2023        GRAD        fT/cm   false   
     240    MEG 2031        MAG         fT      false   
     241    MEG 2032        GRAD        fT/cm   false   
     242    MEG 2033        GRAD        fT/cm   false   
     243    MEG 2041        MAG         fT      false   
     244    MEG 2042        GRAD        fT/cm   false   
     245    MEG 2043        GRAD        fT/cm   false   
     246    MEG 2111        MAG         fT      false   
     247    MEG 2112        GRAD        fT/cm   false   
     248    MEG 2113        GRAD        fT/cm   false   
     249    MEG 2121        MAG         fT      false   
     250    MEG 2122        GRAD        fT/cm   false   
     251    MEG 2123        GRAD        fT/cm   false   
     252    MEG 2131        MAG         fT      false   
     253    MEG 2132        GRAD        fT/cm   false   
     254    MEG 2133        GRAD        fT/cm   false   
     255    MEG 2141        MAG         fT      false   
     256    MEG 2142        GRAD        fT/cm   false   
     257    MEG 2143        GRAD        fT/cm   false   
     258    MEG 2211        MAG         fT      false   
     259    MEG 2212        GRAD        fT/cm   false   
     260    MEG 2213        GRAD        fT/cm   false   
     261    MEG 2221        MAG         fT      false   
     262    MEG 2222        GRAD        fT/cm   false   
     263    MEG 2223        GRAD        fT/cm   false   
     264    MEG 2231        MAG         fT      false   
     265    MEG 2232        GRAD        fT/cm   false   
     266    MEG 2233        GRAD        fT/cm   false   
     267    MEG 2241        MAG         fT      false   
     268    MEG 2242        GRAD        fT/cm   false   
     269    MEG 2243        GRAD        fT/cm   false   
     270    MEG 2311        MAG         fT      false   
     271    MEG 2312        GRAD        fT/cm   false   
     272    MEG 2313        GRAD        fT/cm   false   
     273    MEG 2321        MAG         fT      false   
     274    MEG 2322        GRAD        fT/cm   false   
     275    MEG 2323        GRAD        fT/cm   false   
     276    MEG 2331        MAG         fT      false   
     277    MEG 2332        GRAD        fT/cm   false   
     278    MEG 2333        GRAD        fT/cm   false   
     279    MEG 2341        MAG         fT      false   
     280    MEG 2342        GRAD        fT/cm   false   
     281    MEG 2343        GRAD        fT/cm   false   
     282    MEG 2411        MAG         fT      false   
     283    MEG 2412        GRAD        fT/cm   false   
     284    MEG 2413        GRAD        fT/cm   false   
     285    MEG 2421        MAG         fT      false   
     286    MEG 2422        GRAD        fT/cm   false   
     287    MEG 2423        GRAD        fT/cm   false   
     288    MEG 2431        MAG         fT      false   
     289    MEG 2432        GRAD        fT/cm   false   
     290    MEG 2433        GRAD        fT/cm   false   
     291    MEG 2441        MAG         fT      false   
     292    MEG 2442        GRAD        fT/cm   false   
     293    MEG 2443        GRAD        fT/cm   false   
     294    MEG 2511        MAG         fT      false   
     295    MEG 2512        GRAD        fT/cm   false   
     296    MEG 2513        GRAD        fT/cm   false   
     297    MEG 2521        MAG         fT      false   
     298    MEG 2522        GRAD        fT/cm   false   
     299    MEG 2523        GRAD        fT/cm   false   
     300    MEG 2531        MAG         fT      false   
     301    MEG 2532        GRAD        fT/cm   false   
     302    MEG 2533        GRAD        fT/cm   false   
     303    MEG 2541        MAG         fT      false   
     304    MEG 2542        GRAD        fT/cm   false   
     305    MEG 2543        GRAD        fT/cm   false   
     306    MEG 2611        MAG         fT      false   
     307    MEG 2612        GRAD        fT/cm   false   
     308    MEG 2613        GRAD        fT/cm   false   
     309    MEG 2621        MAG         fT      false   
     310    MEG 2622        GRAD        fT/cm   false   
     311    MEG 2623        GRAD        fT/cm   false   
     312    MEG 2631        MAG         fT      false   
     313    MEG 2632        GRAD        fT/cm   false   
     314    MEG 2633        GRAD        fT/cm   false   
     315    MEG 2641        MAG         fT      false   
     316    MEG 2642        GRAD        fT/cm   false   
     317    MEG 2643        GRAD        fT/cm   false   
     318    STIM 1          MRK         V       false   
     319    STIM 2          MRK         V       false   
     320    STIM 3          MRK         V       false   
     321    STIM 4          MRK         V       false   
     322    STIM 5          MRK         V       false   
     323    STIM 6          MRK         V       false   
     324    STIM 7          MRK         V       false   
     325    STIM 8          MRK         V       false   
     326    STIM 101        MRK         V       false   
     327    SYST 201        SYST        none    false

Plot signal:
```julia
iview(meg)
```

![](images/meg.png)

Plot PSD of gradiometers:
```julia
plot_psd(meg, ch="grad", type=:mean)
```

![](images/meg-psd.png)

Plot PSD of magnetometers:
```julia
p = plot_psd(meg, ch="mag", type=:topo)
```

![](images/meg-psd_topo.png)

Generate and apply embedded SSP projections:
```julia
apply_ssp_projectors!(meg, proj=[1, 2, 3])
```