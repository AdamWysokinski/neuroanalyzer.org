---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Plot (5)

Plot filter response: FIR low-pass (25 Hz) filter, attenuation 60 dB (order × 8), window generated automatically:
```julia
p = filter(eeg, fprototype=:fir, ftype=:lp, cutoff=25, order=15, preview=true)
plot_save(p, file_name="images/fir_lp_25.png")
```

(!) When `preview=true`, signal is not being filtered.

Output:

    [ Info: Creating LP filter:
    [ Info:         Using default window: hamming(128)
    [ Info:         Attenuation: 60 dB
    [ Info:         F_pass: 25.0 Hz
    [ Info:         F_stop: 31.25 Hz
    [ Info:         Transition bandwidth: 0.0244 Hz
    [ Info:         Cutoff frequency: 24.9878 Hz

![](images/fir_lp_25.png)

Plot filter response: Remez FIR high-pass 4 Hz) filter, band width 2:
```julia
p = filter(eeg, fprototype=:remez, ftype=:hp, cutoff=4, bw=2, preview=true)
plot_save(p, file_name="images/remez_lp_4_2.png")
```

![](images/remez_lp_4_2.png)

Plot filter response: Butterworth IIR band-stop (45-55 Hz) filter, order 8 (default):
```julia
p = plot_filter_response(fs=sr(eeg), n=epoch_len(eeg), fprototype=:butterworth, ftype=:bs, cutoff=(45, 55), order=8)
plot_save(p, file_name="images/butter_bs_45-55_8.png")
```

![](images/butter_bs_45-55_8.png)

Plot filter response: second-order IIR band-stop (50 Hz) filter, band width 8:
```julia
p = plot_filter_response(fs=sr(eeg), n=epoch_len(eeg), fprototype=:iirnotch, cutoff=50, bw=8)
plot_save(p, file_name="images/iir_50_8.png")
```

![](images/iir_50_8.png)
