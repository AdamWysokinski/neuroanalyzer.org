---
title: NeuroAnalyzer changelog
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer stable releases

## v0.25.4 (01/04/2025)

- plot: for some plots (topomap, heatmap, matrix) default palette changed to [bluesreds](https://docs.juliaplots.org/dev/generated/colorschemes/#misc)
- `exclude_bads` preference was added (default is false) to automatically ignore channels marked as bad from analysis
- tutorials are from now on automatically generated using Quarto-based Markdown notebooks
- code cleanup
- many minor additions and fixes

## v0.25.3 (01/03/2025)

- Julia ≥1.11 is required
- greedy threads scheduler is used: significant speed increase of many functions
- statistical functions moved to ([NeuroStats.jl](https://codeberg.org/AdamWysokinski/NeuroStats.jl))
- code cleanup
- many minor additions and fixes

## v0.25.2 (01/02/2025)

- gui: `ifilter()` -- interactive filter design
- code cleanup
- many minor additions and fixes

## v0.25.1 (01/01/2025)

- io: `import_ft()` imports EEG channels data for MEG+EEG recordings
- analyze: `lat_idx()` -- calculates lateralization index
- analyze: `tpt_detect()` and `tpt_analyze()` -- detects and analyzes pinches in TPT recordings
- recorder: `tpt()` -- records Two-point Pinch Test
- channels list may now be defined using Julia regex, e.g. `plot(eeg, ch=r"Fp.*")`
- new data types: TPT
- code cleanup
- many minor additions and fixes

## v0.24.12 (01/12/2024)

- io: `import_ft()` -- FieldTrip EEG, MEG, events
- io: `import_fiff()` imports SSP projections
- gui: `iedit()` -- move locs origin
- process: `generate_ssp_projectors()` and `apply_ssp_projectors()`
- code cleanup
- many minor additions and fixes

Breaking changes:

- markers field `description` renamed to `value`
- MEG recording header must contain SSP labels, SSP channels and SSP data fields

## v0.24.11 (01/11/2024)

- plot: `plot_heatmap()`
- tested against Julia 1.11 release
- code cleanup
- many minor additions and fixes

## v0.24.10 (01/10/2024)

- return types added to all functions for better error handling
- code cleanup
- many minor additions and fixes

## v0.24.9 (01/09/2024)

- gui: `iview()`:
    - butterfly / mean mode
    - time/epoch slider
- gui: `ipsd()`:
    - time/epoch slider
    - new channel selector
- gui: `ispectrogram()`:
    - time/epoch slider
    - new channel selector
- gui: `itopo()`:
    - time/epoch entry field
    - compatible with EEG/MEG
- gui: `iview_ica()`:
    - preview and remove ICA components
- code cleanup
- many minor additions and fixes

## v0.24.8 (01/08/2024)

- **breaking change**: channels are addressed by their labels, not numbers; to convert numbers to labels, use `get_channel(obj, type="eeg")` or `get_channel(obj, ch=["Fp1", "Fp2"])`
- **breaking change**: channels are stored as they are in the recording (physical order); list of channels sorted by their type is stored in `OBJ.header.recording[:channel_order]` (logical order)
- **breaking change**: channels label is stored in `OBJ.header.recording[:label]`, channel unit is stored in `OBJ.header.recording[:unit]`
- list of bad channels is stored in the `OBJ.header.recording[:bad_channel]`
- utils: `get_channel()`
    - channels may be addressed by name, e.g. `ch="Fp1"` or `ch=["C3", "C4"]` or by type, e.g. `ch="all"`, `ch="eeg"` or `ch=["mag", "grad"]`
    - supports exclude options (default value is "bad", so bad channels are excluded from the list)
- initial support for MEG data
- io: `import_fiff()` -- both MEG and EEG data
- gui: `iview()` -- lots of changes:
    - select time segment
    - delete time segment / epoch
    - left click channel label to display its properties
    - right click channel label to mark it as bad
    - zoom in and out
    - scroll with mouse wheel
    - toggle scale bars, monochromatic mode, snapping
- plot: `plot_signal()` -- lots of changes:
    - finally displays properly data with > 100 channels
    - gets list of bad channels from the header
    - channels of different type are not displayed separately, but in logical order
    - scale bars and units are drawn for each channel type
- process: `denoise_cwt()` -- denoise using continuous wavelet transformation
- process: `remove_cwt()` -- remove artifacts using continuous wavelet transformation
- analyze: `cwtspectrogram()` -- improved CWT scaleogram
- analyze: `psd()` -- new methods: `:gh`, `:cwt`
- code cleanup
- many minor additions and fixes

## v0.24.7 (01/07/2024)

- stim: `tdcs_dose()`, `tacs_dose()`, `tpcs_dose()` -- calculate effective parameters of tES dosages
- process: `filter_create()` -- new filter type: weighted least-squares FIR filter
- gui: `iview()` can show bad channels
- process: `erp()` -- baseline segment is defined by two time points, may be subtracted before or after averaging
- analyze: PSD- and spectrogram-, EROS-, EROP-related functions -- log scaling of frequencies removed, moved to plotting functions
- code cleanup
- many minor additions and fixes

## v0.24.6 (01/06/2024)

- recorder: `eda()` and `ieda()` -- record electrodermal activity using GSR sensor and Arduino via serial port
- recorder: `ftt()` and `iftt()` -- can record data from a console attached to Raspberry Pi via GPIO pin or to Raspberry Pi/Arduino via serial port
- edit: `add_channel()` and `add_chanel!()` -- channel(s) may be added to an object already containing channels 
- analyze: `symmetry()` -- calculate signal symmetry
- misc: `iplay()` and `play()` -- play signal as audio file
- code cleanup
- many minor additions and fixes

## v0.24.5 (01/05/2024)

- process: `mlinterpolate_channel()` -- interpolate bad channel using MLJ regressor
- process: use reference epochs for `mlinterpolate_channel()` and `lrinterpolate_channel()`
- edit: `subepoch()` -- extract sub-epochs of reduced time range
- plugins: `na_plugin_install()` -- plugins can be installed from a local .zip/.tar.gz archive or using Git repository URL
- code cleanup
- many minor additions and fixes

## v0.24.4 (01/04/2024)

- NeuroAnalyzer has been added to [JuliaHealth](https://github.com/JuliaHealth)
- submodule NeuroTester added -- design and run psychological studies
- submodule NeuroStim added -- process and model neurostimulation techniques
- recorder: `ftt()` -- Finger Tapping Test (CLI mode)
- analyze: `cpsd()` -- cross-power spectral density
- analyze: `pacf()` -- partial auto-correlation
- analyze: `coherence()`
- plot: `plot_coherence()`
- code cleanup
- many minor additions and fixes

## v0.24.3 (01/03/2024)

- submodule NeuroRecorder added -- record neurophysiological data
- recorder: `iftt()` -- Finger Tapping Test
- gui: `iselect_seg()` -- select matrix segment
- analyze: `hjorth()`
- analyze: `peak_frq()`
- analyze: `sef()` -- spectral edge frequency
- analyze: `phsd()` -- phase spectral density
- analyze: `axc2frq()` -- detect peaks in auto-/cross- correlation/covariance and transform them into frequencies
- plot: `plot_phsd()`
- statistics: `seg_extract()` -- extract rectangular or circular segment from a matrix
- code cleanup
- many minor additions and fixes

## v0.24.2 (01/02/2024)

- NeuroAnalyzer is registered in the Julia general registry
- new data types: iEEG, sEEG, ECoG
- new channel types: `ieeg`, `seeg`, `ecog`
- io: `import_ncs()`
- io: `import_xdf()`
- gui: `itopo()`
- gui: `iselect_ts()`
- plots: `plot_ci()`
- analysis: `gradient()`
- analysis: biased or unbiased auto-/cross- correlation/covariance
- edit: `sort_epochs()`
- statistics: many new statistical functions
- code cleanup
- many minor additions and fixes

## v0.24.1 (01/01/2024)

- Julia ≥1.10 is required
- new data type: MEP
- io: `import_cnt()`
- io: `import_duomag()`
- io: `import_thymatron()`
- io: `load_locs_dat()`
- io: `load_locs_asc()`
- io: detect montage when importing EEG files
- plots: `plot_mep()`
- analysis: `mep_peaks()`
- process: improved artifacts detection (using `tkeo()`)
- process: rename channels when referencing
- gui: `iview()` use mouse wheel to scroll channels
- code cleanup
- many minor additions and fixes

## v0.23.12 (01/12/2023)

- locs: `locs_generate()` -- generate spherical coordinates according to 10/10 system
- io: `import_npy()`
- statistics: `ci_median()`, `ci_r()`, `ci2z()`, `r_test()`
- code cleanup
- many minor additions and fixes

## v0.23.11 (01/11/2023)

- gui: `iplot_icatopo()` -- interactive ICA viewer
- gui: `iview()` plots components and two NEURO objects
- gui: `iedit()` snapping to time points
- edit: `delete_optode()`
- locs: `load_locs()` generates locs for EMG/EOG/M1/M2/A1/A2 electrodes
- new data type: sensors
- code cleanup
- many minor additions and fixes

## v0.23.10 (01/10/2023)

- gui: `iedit_ch()` -- interactive channel and locations editor
- gui: `iview_plot()` -- interactive plot viewer
- gui: `iplot_locs3d()` -- interactive 3D locs preview
- gui: improved `iview()`, `iplot()`, `ipsd()`, `ispectrogram()`
- locs: improved locs architecture and functions
- plot: rewritten `plot_locs()`, `plot_topo()`, `plot_weights()` and `plot_connections()` functions -- locs can be projected in 3 planes (XY, XZ and YZ)
- plot: `add_locs()` -- locs preview can be placed over another plot
- analysis: improved methods of calculating PSD and spectrogram
- fix: FFTW provider on non-Intel/AMD architectures
- plot: GLMakie dependencies removed -- significantly reduced startup time
- code cleanup
- many minor additions and fixes

## v0.23.9 (01/09/2023)

- updated interactive editor
- plot: `iplot()`, `ipsd()`, `ispectrogram()`
- plot: plot PSD and spectrograms for a time segment
- initial SEEG support
- IO: `import_montage()`
- IO: `import_nwb()` (WIP)
- created `lite` branch (with graphical functions removed)
- code cleanup
- many minor additions and fixes

## v0.23.8 (01/08/2023)

- license changed to 2-clause BSD
- analysis: heart rate variability `hrv_detect()`, `hrv_analyze()`
- process: `ica_decompose()`, `ica_reconstruct()`, `ica_remove()`
- plot: `plot_icatopo()`
- plot: plot(obj1, obj2)
- plot: preview(obj1, obj2)
- locs: `locs_rotx()`, `locs_roty()`, `locs_rotz()`
- tested against upcoming Julia 1.10 release
- code cleanup
- many minor additions and fixes

## v0.23.7 (01/07/2023)

- plot: `preview()`: interactive (Gtk-based) preview and edit epoched/continuous signal
- process: `remove_powerline()`
- process: `remove_pops()`
- initial support for ECoG data
- Docker container
- code cleanup
- many minor additions and fixes

## v0.23.6 (01/06/2023)

- edit: `create()`
- edit: `join()` and `join!()`
- analyze: `acor()`, `xcor()`, `acov()`, `xcov()`
- Julia ≥ 1.9.0 is now required
- code cleanup
- many minor additions and fixes

## v0.23.5 (01/05/2023)

- new data type: ERP
- IO: GDF files
- IO: improved `import_set()` (epochs, locs, markers)
- IO: improved `import_bv()`
- IO: `import_edf()` accepts multi-sampling rate files
- analyze: `amp_at()` and `avgamp_at()` for ERPs
- edit: `ch2marker()`
- utils: `to_df()`
- time segments are in seconds
- code cleanup
- more tutorials
- many minor additions and fixes

## v0.23.4 (01/04/2023)

- complete rewriting of NeuroAnalyzer functions
- new architecture allows easy expanding with new types of neurophysiological data
- new data types: EEG, NIRS, MEG
- new channel types (csd, nirs, mag, grad, meg)
- IO: NIRS/SNIRF/NIRX files (NIRS)
- IO: MEG FIFF files (very WIP)
- process: convert intensity to optical density (NIRS)
- process: convert optical density to HbO/HbR/HbT concentrations (NIRS)
- plot: new way of plotting channels of different types
- plot: 2D dipole plot (WIP)
- unit tests were completely rewritten
- code cleanup
- more tutorials
- many minor additions and fixes

## v0.23.3 (01/03/2023)

- IO: EEGLAB .set files (WIP)
- IO: CSV files
- IO: GEO electrode locations
- process: transform using surface Laplacian
- process: improved FIR filter (automated filtering window)
- process: improved polynomial filter
- plot: 3D dipole plot (WIP)

## v0.23.2 (01/02/2023)

- import Philips Alice4 polysomnography EDF (non-standard, multi-sampling rate)
- user defined number of Slepian tapers for PSD
- processing of electrode locations updated, added rotating and scaling
- code cleanup
- more tutorials
- many minor additions and fixes

## v0.23.1 (01/01/2023)

- automated epoching by event markers
- ERP: create, plot
- interpolate EEG channel using linear regression
- continuous integration (CI): tests, documentation, website
- more tutorials
- many minor additions and fixes

## v0.22.12 (01/12/2022)

- plotting functions were completely rewritten
- new plot types added (multi-channel PSD, ``eeg_plot_stats()``)
- import BrainVision
- tutorials at [https://neuroanalyzer.org/#tutorials](https://neuroanalyzer.org/#tutorials)
- `eeg_*()` functions now operate on specified channels
- improved auto-detection of bad channels/epochs
- many minor additions and fixes

## v0.22.11 (01/11/2022)

- user-managed preferences
- virtual channels
- progress meter
- continuous and discrete wavelet transformations
- Hilbert transformation envelope
- improved connectivity plot
- proper handling of negative frequencies in FFT spectrum
- Jupyter notebook
- many minor additions and fixes

## v0.22.10 (01/10/2022)

- project website: [https://neuroanalyzer.org](https://neuroanalyzer.org)
- IO: Digitrack, CSD
- Pluto notebook
- study object (WIP)
- automatically process only EEG/MEG signals where necessary (WIP)
- new logo
- many minor additions and fixes

## v0.22.9 (01/09/2022)

- name changed to NeuroAnalyzer to comply with Julia package naming policy
- IO: EDF+, BDF and BDF+ with annotations
- view/edit annotations
- interactive 3D preview of electrodes
- PSD topomap
- edit electrode locations
- automated processing of signal channels only (WIP)
- benchmarking module (WIP)
- many minor additions and fixes

## v0.22.8 (01/08/2022)

- initial CUDA support
- new reference mode: simple (planar) Laplacian
- plugins use git
- many minor additions and fixes

## v0.22.7 (01/07/2022)

- first official release

# Git commits

[Stable branch](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/commits/branch/stable)

[Devel branch](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/commits/branch/devel)