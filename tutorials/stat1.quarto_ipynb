{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "---\n",
        "jupyter: julia-1.11\n",
        "last-modified:\n",
        "pagetitle: \"NeuroAnalyzer tutorial\"\n",
        "format:\n",
        "  html:\n",
        "    code-copy: false\n",
        "    css: rethink.css\n",
        "    embed-resources: true\n",
        "    smooth-scroll: true\n",
        "    theme: darkly\n",
        "    highlight-style: ayu-dark\n",
        "    page-layout: full\n",
        "---\n",
        "\n",
        "<center>\n",
        "![](images/neuroanalyzer.png)\n",
        "</center>\n",
        "\n",
        "# NeuroAnalyzer tutorials: Statistics (1)\n",
        "\n",
        "Load data:\n"
      ],
      "id": "deec685f"
    },
    {
      "cell_type": "code",
      "metadata": {},
      "source": [
        "using NeuroAnalyzer\n",
        "using NeuroStats\n",
        "using Plots\n",
        "eeg = load(\"eeg.hdf\")"
      ],
      "id": "e46455a5",
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "(!) Most statistial functions used by the NeuroAnalyzer are in the separate package: [NeuroStats.jl](https://codeberg.org/AdamWysokinski/NeuroStats.jl).\n",
        "\n",
        "## Calculate and plot CI using bootstrapping\n",
        "\n",
        "Get 1st channel for all epochs and calculate its 95%CI\n"
      ],
      "id": "f6bc8248"
    },
    {
      "cell_type": "code",
      "metadata": {},
      "source": [
        "s = eeg.data[1, :, :]\n",
        "t = eeg.epoch_time\n",
        "s_avg, s_l, s_u = bootstrap_ci(s, ci=0.95)"
      ],
      "id": "01a4dc24",
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "Plot CI:\n"
      ],
      "id": "3ce790aa"
    },
    {
      "cell_type": "code",
      "metadata": {},
      "source": [
        "plot_ci(s_avg, s_l, s_u, t)"
      ],
      "id": "09c72235",
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "## Calculate statistic using bootstrapping\n",
        "\n",
        "Any statistical function may be calculated using bootstrapping method. The function must be provided as `f=\"function_name(obj)\"`, obj will be replaced with the signal data, e.g. `f=\"mean(obj)\"`.\n"
      ],
      "id": "3f9fb3a1"
    },
    {
      "cell_type": "code",
      "metadata": {},
      "source": [
        "s = eeg.data[1, :, :]\n",
        "s_stat = abs(maximum(s))\n",
        "s_dist  = bootstrap_stat(s, f=\"abs(maximum(obj))\")"
      ],
      "id": "89c06009",
      "execution_count": null,
      "outputs": []
    },
    {
      "cell_type": "code",
      "metadata": {},
      "source": [
        "plot_histogram(s_dist, s_stat, draw_median=true, draw_mean=false)"
      ],
      "id": "4ff0ff7d",
      "execution_count": null,
      "outputs": []
    }
  ],
  "metadata": {
    "kernelspec": {
      "name": "julia-1.11",
      "language": "julia",
      "display_name": "Julia 1.11.1",
      "path": "/home/eb/.local/share/jupyter/kernels/julia-1.11"
    }
  },
  "nbformat": 4,
  "nbformat_minor": 5
}