---
jupyter: julia-1.11
last-modified:
pagetitle: "NeuroAnalyzer tutorial"
format:
  html:
    code-copy: false
    css: rethink.css
    embed-resources: true
    smooth-scroll: true
    theme: darkly
    highlight-style: ayu-dark
    page-layout: full
---
<center>
![](images/neuroanalyzer.png)
</center>

# NeuroAnalyzer tutorials: plugins

NeuroAnalyzer functionalities can be extended using plugins (extensions). Plugins are stored in the `$HOME/NeuroAnalyzer/plugins` folder. Each plugins must be located in its individual folder, inside the `src` subfolder. The structure of the plugins folder is:

```
$HOME/NeuroAnalyzer/
|- pipelines
|- plugins
   |- na_test_plugin
      |- src
         |- na_test_plugin.jl
   |- plugin_1
      |- src
         |- plugin1.jl
   |- plugin_2
      |- src
         |- plugin2.jl
```

To list all installed plugins:

```{julia}
using NeuroAnalyzer
na_plugins_list()
```

To reload all installed plugins:

```{julia}
na_plugins_reload()
```

Plugins are installed from Git repositories. To install a new plugin:

```{julia}
#| eval: false
url = "https://codeberg.org/AdamWysokinski/plot_ITPC.jl"
na_plugins_install(url)
```

Plugins may also be installed from local .ZIP/.TAR.GZ archives:

```{julia}
#| eval: false
url = "plot_ITPC.jl_main.zip"
na_plugins_install(url)
```

(!) Installing from local archive requires `unzip` or `tar`.

(!) Files will be overwritten when installing from local archive.

To update plugins:

```{julia}
na_plugins_update()
```

Individual plugins may also be updated using:

```{julia}
#| eval: false
na_plugins_update("plot_ITPC")
```
