---
jupyter: julia-1.11
last-modified:
pagetitle: "NeuroAnalyzer tutorial"
format:
  html:
    code-copy: false
    css: rethink.css
    embed-resources: true
    smooth-scroll: true
    theme: darkly
    highlight-style: ayu-dark
    page-layout: full
---
<center>
![](images/neuroanalyzer.png)
</center>

# NeuroAnalyzer tutorials: Edit EEG (2)

## Edit channels

Load data:

```{julia}
using NeuroAnalyzer
eeg = import_edf("files/eeg_edfplus.edf");
```

Show labels:

```{julia}
labels(eeg)
```

Get channel by number:

```{julia}
get_channel(eeg, ch=labels(eeg)[18])
```

Rename channels:

```{julia}
rename_channel(eeg, ch="Cz", name="CZ");
labels(eeg)
```

Delete channels (epochs and channels may be specified using number, range or vector):

```{julia}
delete_channel(eeg, ch="Cz");
labels(eeg)
```

or

```{julia}
delete_channel(eeg, ch=get_channel(eeg, type="eeg")[10:18]);
labels(eeg)
```

Keep channel:

```{julia}
keep_channel(eeg, ch=get_channel(eeg, type="eeg")[1:4])
```

Replace channel 1 with channel 18:

```{julia}
ch18 = extract_channel(eeg, ch=get_channel(eeg, type="all")[18])
replace_channel(eeg, ch=get_channel(eeg, type="all")[1], s=ch18)
```

## Epoching

(!) Many processing operations (such as filtering or ICA decomposition) should be performed on non-epoched (continuous) signal (e.g. due to the edge artifacts that may occur in case of filtering or the minimum required signal length for ICA decomposition).

Split EEG into 10-second epochs:

```{julia}
e10 = epoch(eeg, ep_len=10)
```

Split into 5-second epochs and then average into one epoch:

```{julia}
e5 = epoch(eeg, ep_len=5)
e5avg = average_epochs(e5)
info(e5avg)
```

Split into epochs at the event marker (each epoch is 1500 ms long and starts 200 ms before the event marker):

```{julia}
erp = epoch(eeg, marker="Eyes Open", offset=0.2, ep_len=1.5)
```

Create sub-epochs (of reduced time range, start at 0.1 sec and end 1.1 sec):

```{julia}
subepoch(erp, ep_start=0.1, ep_end=1.1)
```

## Edit epochs

Get 1st epoch:

```{julia}
e10e1 = extract_epoch(e10, ep=1)
info(e10e1)
```

Delete epochs:

```{julia}
delete_epoch(e10, ep=1)
delete_epoch(e10, ep=8:10)
delete_epoch(e10, ep=[1:5; 9; 10])
```
(!) Note the use of `;` if mixed range is specified.

Keep epochs:

```{julia}
keep_epoch(e10, ep=[1, 3, 5, 9])
```

## Using markers

View markers:

```{julia}
view_marker(eeg)
```

(!) Value of `channel` refers to channels affected by the marker; 0 means all channels; values other than 0 are not supported.

Delete 1st marker:

```{julia}
delete_marker(eeg, n=1)
```

Edit 1st marker:

```{julia}
edit_marker(eeg, n=1, id="Note", start=1, len=1, value="test")
```

Add marker at position `start` (in seconds):

```{julia}
add_marker(eeg, id="Note", start=1, len=1, value="test")
```