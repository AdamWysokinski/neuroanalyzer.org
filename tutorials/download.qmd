---
jupyter: julia-1.11
last-modified:
pagetitle: "NeuroAnalyzer tutorial"
format:
  html:
    code-copy: false
    css: rethink.css
    embed-resources: true
    smooth-scroll: true
    theme: darkly
    page-layout: full
---
<center>
![](images/neuroanalyzer.png)
</center>

# NeuroAnalyzer tutorials: Download and install

Note: this toolbox is under active development and is subject to change.

First, download latest stable [Julia](https://julialang.org/downloads/) version. 

## Multi-threading

Please note that Julia uses the `JULIA_NUM_THREADS` environmental variable to determine the number of threads available for the program.

For optimal performance this number should be > 1 and, depending upon your CPU number of cores, usually is equal or more than the number of cores.

For Unix-based systems (Linux, FreeBSD, MacOS) this may be set in the `~/.profile` file or `~/.zshrc` or `~/.bashrc` (depending on your shell).

```{sh}
export JULIA_NUM_THREADS=12
```

The easiest way (which also works in Windows) it to set this variable in the Julia `startup.jl` file, located in `~/.julia/config` directory:

```{julia}
#| eval: false
ENV["JULIA_NUM_THREADS"] = 12
ENV["JULIA_NUM_PRECOMPILE_TASKS"] = 12
```

(!) Setting the `JULIA_NUM_PRECOMPILE_TASKS` variable speeds up packages precompilation process.

(!) Note that the 32-bit x86 architecture does not support threading due to lack of compiler runtime library support, so you may need to set `JULIA_NUM_THREADS=0` and `JULIA_NUM_PRECOMPILE_TASKS=0` if you're on a 32-bit system.

(!) If you encounter the following error: `dyld: Library not loaded: @rpath/libLLVM.dylib at pre-compilation`, start Julia using:

```{sh}
julia --pkgimages=no
```

## Installation using package manager

Since version 0.24.1, NeuroAnalyzer is registered in the Julia general registry. Therefore, the easiest way of installing the package is to use `Pkg.add()`:

```{julia}
#| eval: false
using Pkg
Pkg.add("NeuroAnalyzer")
```

You may also manually add NeuroAnalyzer using Julia package manager:

```{julia}
#| eval: false
using Pkg
Pkg.update()
Pkg.add(url="https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl")
Pkg.resolve() # if necessary
Pkg.update()
```

## Installation using environment

Another option is to initialize a new Julia environment for the package:

```{sh}
git clone https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl
cd NeuroAnalyzer.jl
julia --project
# or (in which case you may ignore the line `Pkg.activate(@__DIR__)` below)
julia
```

Activate the package in the Julia REPL:

```{julia}
#| eval: false
using Pkg
Pkg.activate(@__DIR__)
Pkg.instantiate()
Pkg.resolve() # if necessary
Pkg.update()
```

## Downloading zipped release

Alternatively, you may download NeuroAnalyzer [latest release](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/releases):

```{sh}
unzip NeuroAnalyzer-*.zip
cd NeuroAnalyzer-*
julia --project
# or (in which case you may ignore the line `Pkg.activate(@__DIR__)` below)
julia
```

Activate the package in the Julia REPL:

```{julia}
#| eval: false
using Pkg
Pkg.activate(@__DIR__)
Pkg.instantiate()
Pkg.resolve() # if necessary
Pkg.update()
```

## Installation using LOAD_PATH variable

The third option is to extend the Julia `LOAD_PATH` variable in the `~/.julia/config/startup.jl` with the NeuroAnalyzer folder (downloaded as cloned or zipped repository):

```{julia}
#| eval: false
push!(LOAD_PATH, "path/to/NeuroAnalyzer/folder")
```

## Running NeuroAnalyzer

Start Julia and in the Julia REPL do the following:

```{julia}
using NeuroAnalyzer
```

Due to the nature of Julia compiler, initial startup of NeuroAnalyzer takes several seconds. To speedup the process, creating a custom sysimage for reduced latency using [PackageCompiler.jl](https://github.com/JuliaLang/PackageCompiler.jl) is recommended. See [this tutorial](tut-sysimage.html) how to do it.

Show version and info (for reproducibility):

```{julia}
na_version()
```

```{julia}
na_info()
```

Get help:

```{julia}
#| eval: false
?NeuroAnalyzer.plot
```

For interactive GUI, use Pluto and PlutoUI:

```{julia}
#| eval: false
using Pkg
Pkg.add("Pluto")
Pkg.add("PlutoUI")
Pkg.add("Gtk")
using Pluto
Pluto.run()
```

Example Pluto notebook is located [here](https://codeberg.org/AdamWysokinski/NeuroAnalyzer-notebooks/src/branch/main/Notebook.jl).

For non-interactive GUI, use Jupyter:

```{julia}
#| eval: false
using Pkg
Pkg.add("IJulia")
using IJulia
notebook()
```

Example Jupyter notebook is located [here](https://codeberg.org/AdamWysokinski/NeuroAnalyzer-notebooks/src/branch/main/Notebook.ipynb).

# Docker image

NeuroAnalyzer is also available as the Docker image:

```{sh}
docker pull neuroanalyzer/neuroanalyzer:latest
xhost local:docker
docker run -e DISPLAY=:0 -v /tmp/.X11-unix:/tmp/.X11-unix -it neuroanalyzer
```
