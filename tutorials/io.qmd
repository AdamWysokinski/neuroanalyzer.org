---
jupyter: julia-1.11
last-modified:
pagetitle: "NeuroAnalyzer tutorial"
format:
  html:
    code-copy: false
    css: rethink.css
    embed-resources: true
    smooth-scroll: true
    theme: darkly
    highlight-style: ayu-dark
    page-layout: full
---
<center>
![](images/neuroanalyzer.png)
</center>

# NeuroAnalyzer tutorials: Importing/exporting data

## Import

The following data formats are supported:

- EDF (.edf)
- EDF+ (.edf)
- Alice4 EDF (.edf)
- BDF (.bdf)
- BDF+ (.bdf)
- GDF (.gdf)
- DigiTrack (.ascii)
- BrainVision (.vhdr)
- CSV (.csv): channels × time or time × channels data tables are recognized automatically
- EEGLAB (.set): preliminary support
- FieldTrip (.mat): EEG, MEG, NIRS, events -- preliminary support
- FIFF (.fif|.fiff): preliminary support
- Neuroscan continuous signal (.cnt)
- Neuralinx Continuously Sampled Channels (CSC) (.ncs)
- Neurodata Without Borders (.nwb)
- Extensible Data Format (.xdf)
- NIRS (.nirs)
- SNIRF (.snirf)
- NIRX (.nirx)
- DuoMAG MEP (.ascii|.m)

Load recording file -- proper importer is recognized based on file extension:

```{julia}
using NeuroAnalyzer
eeg = import_recording("files/eeg.edf")
```

By default, channel types (EEG, ECG, EOG, etc.) are recognized automatically based on channel labels. This may not work for some recordings with incorrect channel names. Channel type detection can be disabled using `detect_type::Bool` parameter:

```{julia}
eeg = import_recording("files/eeg.edf")
eeg.header.recording[:channel_type]
```

```{julia}
eeg = import_recording("files/eeg.edf", detect_type=false)
eeg.header.recording[:channel_type]
```

Load EDF/EDF+:

```{julia}
#| eval: false
eeg = import_edf("files/eeg-test-edf.edf")
eeg = import_edf("files/eeg-test-edfplus.edf")
```

Load Alice4 EDF:

```{julia}
#| eval: false
eeg = import_alice4("files/eeg-test-alice4.edf")
```

Load BDF/BDF+:

```{julia}
#| eval: false
eeg = import_recording("files/eeg-test-bdf.bdf")
eeg = import_recording("files/eeg-test-bdfplus.bdf")
```

Load GDF:

```{julia}
#| eval: false
eeg = import_recording("files/eeg-test-gdf_1.25.gdf")
eeg = import_recording("files/eeg-test-gdf_2.20.gdf")
```

Load DigiTrack:

```{julia}
#| eval: false
eeg = import_digitrack("files/eeg-test-digitrack.txt")
```

Load BrainVision (at least two files must be available: .vhdr/.ahdr and .eeg):

```{julia}
#| eval: false
eeg = import_recording("files/eeg-test-bv.vhdr")
eeg = import_recording("files/eeg-test-bv.ahdr")
```

(!) BrainVision `.eeg` and `.vmrk` (markers, optional) files will be loaded using file names provided in `.vhdr`/`.ahdr`. All three files must be located in the same folder.

Load CSV:

```{julia}
#| eval: false
eeg = import_csv("files/eeg-test_chxt.csv.gz")
eeg = import_csv("files/eeg-test_txch.csv.gz")
```

Load SET:

```{julia}
#| eval: false
eeg = import_set("files/eeg-test-eeglab.set")
```

## Export

Object may be exported to CSV files:

```{julia}
#| eval: false
export_csv(eeg, file_name="files/eeg.csv")
```

(!) By default, only signals are exported.

To export header meta-data, markers, components and locs:

```{julia}
#| eval: false
export_csv(eeg, file_name="files/eeg.csv", header=true, components=true, markers=true, locs=true)
```

## Save

NeuroAnalyzer `NEURO` objects are saved as [HDF5](https://en.wikipedia.org/wiki/Hierarchical_Data_Format) file.

To save the object:

```{julia}
#| eval: false
save(eeg, file_name="files/eeg.hdf")
```

To overwrite, set `overwrite=true`:

```{julia}
#| eval: false
save(eeg, file_name="files/eeg.hdf", overwrite=true)
```

(!) Filename extension must be `.hdf`

## Load

Loading NeuroAnalyzer object from HDF5:

```{julia}
eeg = load("files/eeg.hdf")
```
