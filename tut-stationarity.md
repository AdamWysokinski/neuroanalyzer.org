---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Stationarity

There are several types of stationarity tests available in NeuroAnalyzer.

To test mean stationarity:
```julia
s_m = stationarity(eeg, method=:mean, window=256)
p = plot(s_m[1, :, 1], xlabel="time-window points", label="channel 1, epoch 1")
plot_save(p, file_name="images/eeg_s_m.png")
```

![](images/eeg_s_m.png)

To test variance stationarity:
```julia
s_v = stationarity(eeg, method=:var, window=256)
p = plot(s_v[1, :, 1], xlabel="time-window points", label="channel 1, epoch 1")
plot_save(p, file_name="images/eeg_s_v.png")
```

![](images/eeg_s_v.png)

To test phase stationarity using Hilbert transformation:
```julia
s_p = stationarity(eeg, method=:hilbert, window=256)
p = plot(eeg.epoch_time[1:end-1], s_p[1, :, 1], xlabel="epoch time [s]", label="channel 1, epoch 1", legend=:topright)
plot_save(p, file_name="images/eeg_s_p.png")
```

![](images/eeg_s_p.png)

To test covariance stationarity based on Euclidean distance between covariance matrix of adjacent time windows:
```julia
s_c = stationarity(eeg, method=:cov, window=256)
p = plot(s_c[:, 15], label="epoch 15", ylabel="distance between covariance matrices", xlabel="time-window segment", legend=:topright)
plot_save(p, file_name="images/eeg_s_c.png")
```

![](images/eeg_s_c.png)

To test phase stationarity using Augmented Dickey–Fuller test:
```julia
s_adf = stationarity(eeg, method=:adf, window=256)
s_adf[12, :, 1]
```
The output is ADF = -3.99 and p = 0.0015, meaning that channel 12, epoch 1 signal is stationary.