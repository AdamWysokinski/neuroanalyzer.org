---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroTester: Finger Tapping Test (FTT)

Finger Tapping Test (FTT) is a simple neuropsychological test that examines motor functioning, specifically, motor speed and lateralized coordination.

## ifft()

The simplest way is to operate the test using computer keyboard space key.

To run FTT:
```julia
t = iftt(duration=10, trials=6, interval=5)
```

`iftt()` parameters are:

- `duration`: single trial length (in seconds)
- `trials`: number of trials
- `interval`: length of inter-trial intervals (in second)

To analyze the test results, use:
```julia
ftt_analyze(t)
```

![](images/ftt1.png)

![](images/ftt2.png)

## fft() -- Raspberry Pi via GPIO

It is also possible to use an external panel attached to Raspberry Pi GPIO pins. Reading pin must be provided by its GPIO number (e.g. GPIO 23 is RPi board pin 16). NeuroAnalyzer has to be installed and running on RPi (RPi 4 or higher is recommended due to limited amount of memory in lower models). `pigpiod` is required for running NeuroAnalyzer on RPi (`sudo apt install pigpio-tools; sudo systemctl enable pigpiod; sudo systemctl start pigpiod`).

```julia
ftt(duration=10, trials=6, interval=5, gpio=23)
```

(!) If `ftt()` is not running on RPi, keyboard SPACEBAR key is used.

Output:

    NeuroRecorder: FTT
    ==================
      Trials: 5
    Duration: 5 [seconds]
    Interval: 2 [seconds]
      Button: Button: RPi GPIO 23
    
    Ready to start, press the BUTTON to begin the test
    
    The test will start after a beep
    
       Trial 1: press the BUTTON as quickly as possible
    
    Interval 1: DO NOT press the BUTTON
    
       Trial 2: press the BUTTON as quickly as possible
    
    Interval 2: DO NOT press the BUTTON
    
       Trial 3: press the BUTTON as quickly as possible
    
    Interval 3: DO NOT press the BUTTON
    
       Trial 4: press the BUTTON as quickly as possible
    
    Interval 4: DO NOT press the BUTTON
    
       Trial 5: press the BUTTON as quickly as possible
    
    Interval 5: DO NOT press the BUTTON
    
    Testing completed

(!) If you want to use USB keyboard attached to RPi, please use `ftt(gpio=-1)` option to disable using GPIO.

(!) Running NeuroAnalyzer on RPi via ssh requires using virtual framebuffer:

```
DISPLAY=:0 xvfb-run -a -s '-screen 0 1024x768x24' julia
```

(!) The following error:

```
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   Can't connect to pigpio at localhost:8888
   Did you start the pigpio daemon? E.g. sudo pigpiod
   
   Did you specify the correct Pi host/port in the environment
   variables PIGPIO_ADDR/PIGPIO_PORT?
   E.g. export PIGPIO_ADDR=soft, export PIGPIO_PORT=8888
   
   Did you specify the correct Pi host/port in the
   Pi() function? E.g. Pi('soft', 8888))
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
```

indicates that `pigpiod` is not running, start it using `sudo pigpiod` in Julia shell (press `;` to activate it).

## fft() -- Raspberry Pi/Arduino via serial port

Another option is to use an external panel attached to RPi Pico/Arduino (which is attached to a PC running NeuroAnalyzer via USB cable). RPi/Arduino is running a [daemon](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/src/branch/main/misc/buttons.ino) reading data from GPIO pin and sending results via the serial port:

```julia
ftt(duration=10, trials=6, interval=5, port_name="/dev/ttyACM0", gpio=28)
```

Output:

    NeuroRecorder: FTT
    ==================
      Trials: 5
    Duration: 5 [seconds]
    Interval: 2 [seconds]
      Button: serial port /dev/ttyACM0 GPIO 28
    
    Ready to start, press the BUTTON to begin the test
    
    The test will start after a beep
    
       Trial 1: press the BUTTON as quickly as possible
    
    Interval 1: DO NOT press the BUTTON
    
       Trial 2: press the BUTTON as quickly as possible
    
    Interval 2: DO NOT press the BUTTON
    
       Trial 3: press the BUTTON as quickly as possible
    
    Interval 3: DO NOT press the BUTTON
    
       Trial 4: press the BUTTON as quickly as possible
    
    Interval 4: DO NOT press the BUTTON
    
       Trial 5: press the BUTTON as quickly as possible
    
    Interval 5: DO NOT press the BUTTON
    
    Testing completed

## fft() -- switch debouncing

To minimize switch bounce, one of these circuits may be used when attaching a push button to RPi:

![](images/pi2.png)

![](images/pi1.png)

Also, the [daemon](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/src/branch/main/misc/buttons.ino) running on Raspberry Pico/Arduino minimizes switch bound.