---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: HRV

Heart rate variability (HRV) is the physiological phenomenon of variation in the time interval between heartbeats. It is measured by the variation in the beat-to-beat interval (source: [Wikipedia](https://en.wikipedia.org/wiki/Heart_rate_variability)).

NeuroAnalyzer allows detecting and analysis of HRV using ECG channel.

The following time-domain parameters are analyzed:

- **MENN**: the mean of NN intervals
- **MDNN**: the median of NN intervals
- **VNN**: the variance of NN intervals
- **SDNN**: the standard deviation of NN intervals
- **RMSSD**: ("root mean square of successive differences"), the square root of the mean of the squares of the successive differences between adjacent NNs
- **SDSD**: ("standard deviation of successive differences"), the standard deviation of the successive differences between adjacent NNs
- **NN50**: the number of pairs of successive NNs that differ by more than 50 ms
- **pNN50**: the proportion of NN50 divided by total number of NNs
- **NN20**: the number of pairs of successive NNs that differ by more than 20 ms
- **pNN20**: the proportion of NN20 divided by total number of NNs

```julia
nn_seg, r_idx = hrv_detect(eeg)
```

Output:
                                     
    [ Info: ECG channel found: 24
    [ Info: Detected NN segments: 1288

Check peaks:
```julia
ecg = eeg.data[24, :, :][:]
p = Plots.plot(ecg[1:20*sr(eeg)],               # 20 seconds
               lc=:black,
               lw=0.5,
               legend=false,
               yticks=false,
               xticks=false)
p = Plots.vline!(r_idx[1:21],                   # 21 peaks
                 ls=:dot,
                 lc=:red,
                 alpha=0.5)
plot_save(p, file_name="images/rr.png")
```

![](images/rr.png)

```julia
hrv_analyze(nn_seg)
```

Results:

    menn = 926.176
    mdnn = 937.5
    vnn = 6966.958
    sdnn = 83.468
    rmssd = 68.297
    sdsd = 68.323
    nn50 = 27
    pnn50 = 0.021
    nn20 = 67
    pnn20 = 0.052
