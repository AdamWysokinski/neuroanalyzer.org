---
title: NeuroAnalyzer benchmarks
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer benchmarks

A complete set of NeuroAnalyzer benchmarks is located [here](https://codeberg.org/AdamWysokinski/NeuroAnalyzer-benchmarks).

## NeuroAnalyzer vs MNE and EEGLAB

A simple pipeline in MNE (v. 1.9.0, Python 3.13.2) and EEGLAB (v. 2022.1, MATLAB 2021a + Signal Processing Toolbox) and NeuroAnalyzer: importing 100 EDF files, importing channel locations, HP and LP filtering, common average referencing.

Test system: AMD Ryzen Threadripper 3960X, Debian Linux (6.12.12-amd64 kernel), Julia 1.11.3.

NeuroAnalyzer:
```julia
function na_bench()
    eeg = import_edf("eeg-test-edf.edf")
    load_locs!(eeg, file_name="standard-10-20-cap19-elmiko.locs")
    NeuroAnalyzer.filter!(eeg, ch="all", fprototype=:fir, ftype=:hp, cutoff=0.1)
    NeuroAnalyzer.filter!(eeg, ch="all", fprototype=:fir, ftype=:lp, cutoff=45.0)
    reference_avg!(eeg)
end

# precompile
na_bench()

n = 100
@time for idx in 1:n
    na_bench()
end
```

MNE:
```python
import time
import mne

start = time.time()

n = 100
for idx in range(n):
    raw = mne.io.read_raw_edf("eeg-test-edf.edf", preload=True)
    montage = mne.channels.read_custom_montage("standard-10-20-cap19-elmiko.locs")
    raw.set_montage(montage, match_case=False, on_missing="ignore")
    raw.filter(0.1, None, l_trans_bandwidth='auto', filter_length='auto', phase='zero')
    raw.filter(None, 45.0, h_trans_bandwidth='auto', filter_length='auto',
           phase='zero')
    raw.set_eeg_reference()

end = time.time()
print(end - start)
```

EEGLAB:
```matlab
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
eeglab('redraw');

tStart = tic;
n = 100;
for idx = 1:n
    p(ALLEEG, EEG, CURRENTSET, ALLCOM)
end
tEnd = toc(tStart)

function p(ALLEEG, EEG, CURRENTSET, ALLCOM)
    EEG = pop_biosig('eeg-test-edf.edf');
    EEG = pop_chanedit(EEG, 'lookup','standard-10-20-cap19-elmiko.locs');
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 0,'gui','off'); 
    [ALLEEG EEG] = eeg_store(ALLEEG, EEG, CURRENTSET);
    EEG = pop_eegfiltnew(EEG, 'locutoff',0.1);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 1,'gui','off'); 
    EEG = pop_eegfiltnew(EEG, 'hicutoff',45.0);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 2,'gui','off'); 
    EEG = pop_reref( EEG, []);
    [ALLEEG EEG CURRENTSET] = pop_newset(ALLEEG, EEG, 3,'gui','off'); 
end
```

Results:

- NeuroAnalyzer: 45.2 sec
- MNE: 77.4 sec
- EEGLAB: 133.2 sec

![](images/bench1.png)