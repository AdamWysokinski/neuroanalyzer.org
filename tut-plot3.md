---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Plot (3)

Plot spectrogram (STFT):
```julia
p = plot_spectrogram(eeg, ch=9, db=true)
plot_save(p, file_name="images/eeg_spec1.png")
```

![](images/eeg_spec1.png)

(!) To smooth the histogram, use `smooth=true` option and `n` parameter (kernel size of the Gaussian blur; larger kernel means more smoothing).
```julia
p = plot_spectrogram(eeg, ch=9, db=true, smooth=true, n=5)
```

Plot spectrogram (multi-taper):
```julia
p = plot_spectrogram(eeg, ch=9, db=true, method=:mt)
plot_save(p, file_name="images/eeg_spec6.png")
```

![](images/eeg_spec6.png)

Plot spectrogram (Morlet wavelet):
```julia
p = plot_spectrogram(eeg, ch=9, db=true, method=:mw)
plot_save(p, file_name="images/eeg_spec3.png")
```

![](images/eeg_spec3.png)

Plot spectrogram (Gauss-Morlet):
```julia
p = plot_spectrogram(eeg, ch=9, method=:gh)
plot_save(p, file_name="images/eeg_spec5.png")
```

![](images/eeg_spec5.png)

Plot spectrogram (continuous wavelet transformation):
```julia
p = plot_spectrogram(eeg, ch=9, method=:cwt, wt=wavelet(Morlet(6), β=2, Q=128))
plot_save(p, file_name="images/eeg_spec2.png")
```

![](images/eeg_spec2.png)


Plot multi-channel spectrogram:
```julia
p = plot_spectrogram(eeg, ch=1:19, db=true, frq_lim=(0, 50))
plot_save(p, file_name="images/eeg_spec4.png")
```

![spectrogram mc](images/eeg_spec4.png)
