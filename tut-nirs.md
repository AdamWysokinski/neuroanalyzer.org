---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: NIRS

Load NIRS data:
```julia
n = import_nirs("test/files/fnirs-test-nirs.nirs")
info(n)
```

Output:

                  Data type: NIRS
                File format: NIRS
                Source file: test/files/fnirs-test-nirs.nirs
             File size [MB]: 0.72
           Memory size [MB]: 0.35
                    Subject:  
                  Recording: 
            Recording notes: 
             Recording date: 
             Recording time: 
         Sampling rate (Hz): 25
    Signal length [samples]: 9015
    Signal length [seconds]: 360.6
         Number of channels: 4
           Number of epochs: 1
     Epoch length [samples]: 9015
     Epoch length [seconds]: 360.6
            Wavelength [nm]: [690.0, 830.0]
                     Labels: yes
                    Markers: no
          Channel locations: yes
                 Components: no
    Channels:
            channel: 1      label: S1_D1 690        type: NIRS      wavelength: 690.0 nm
            channel: 2      label: S1_D2 690        type: NIRS      wavelength: 690.0 nm
            channel: 3      label: S1_D1 830        type: NIRS      wavelength: 830.0 nm
            channel: 4      label: S1_D2 830        type: NIRS      wavelength: 830.0 nm

Load SNIRF data:
```julia
n = import_snirf("test/files/fnirs-test-snirf.snirf")
info(n)
```

Output:

                  Data type: NIRS
                File format: SNIRF
                Source file: test/files/fnirs-test-snirf.snirf
             File size [MB]: 1.91
           Memory size [MB]: 0.29
                    Subject: default:  
                  Recording: 
            Recording notes: 
             Recording date: 2022-03-03
             Recording time: 12:03:48
         Sampling rate (Hz): 50
    Signal length [samples]: 500
    Signal length [seconds]: 10.0
         Number of channels: 72
           Number of epochs: 1
     Epoch length [samples]: 500
     Epoch length [seconds]: 10.0
            Wavelength [nm]: [760.0, 850.0]
                     Labels: yes
                    Markers: no
          Channel locations: yes
                 Components: no
    Channels:
            channel: 1      label: S2_D1 760        type: DOD       wavelength: 760.0
            channel: 2      label: S4_D1 760        type: DOD       wavelength: 760.0
            channel: 3      label: S1_D1 760        type: DOD       wavelength: 760.0
            channel: 4      label: S4_D2 760        type: DOD       wavelength: 760.0
            channel: 5      label: S6_D2 760        type: DOD       wavelength: 760.0
            channel: 6      label: S3_D2 760        type: DOD       wavelength: 760.0
            channel: 7      label: S2_D3 760        type: DOD       wavelength: 760.0
            channel: 8      label: S4_D3 760        type: DOD       wavelength: 760.0
            channel: 9      label: S7_D3 760        type: DOD       wavelength: 760.0
            channel: 10     label: S5_D3 760        type: DOD       wavelength: 760.0
            channel: 11     label: S4_D4 760        type: DOD       wavelength: 760.0
            channel: 12     label: S6_D4 760        type: DOD       wavelength: 760.0
            channel: 13     label: S7_D4 760        type: DOD       wavelength: 760.0
            channel: 14     label: S8_D4 760        type: DOD       wavelength: 760.0
            channel: 15     label: S10_D5 760       type: DOD       wavelength: 760.0
            channel: 16     label: S9_D5 760        type: DOD       wavelength: 760.0
            channel: 17     label: S12_D6 760       type: DOD       wavelength: 760.0
            channel: 18     label: S14_D6 760       type: DOD       wavelength: 760.0
            channel: 19     label: S11_D6 760       type: DOD       wavelength: 760.0
            channel: 20     label: S10_D7 760       type: DOD       wavelength: 760.0
            channel: 21     label: S15_D7 760       type: DOD       wavelength: 760.0
            channel: 22     label: S13_D7 760       type: DOD       wavelength: 760.0
            channel: 23     label: S12_D8 760       type: DOD       wavelength: 760.0
            channel: 24     label: S14_D8 760       type: DOD       wavelength: 760.0
            channel: 25     label: S16_D8 760       type: DOD       wavelength: 760.0
            channel: 26     label: S17_D9 760       type: DOD       wavelength: 760.0
            channel: 27     label: S18_D9 760       type: DOD       wavelength: 760.0
            channel: 28     label: S21_D9 760       type: DOD       wavelength: 760.0
            channel: 29     label: S17_D10 760      type: DOD       wavelength: 760.0
            channel: 30     label: S18_D10 760      type: DOD       wavelength: 760.0
            channel: 31     label: S23_D10 760      type: DOD       wavelength: 760.0
            channel: 32     label: S19_D11 760      type: DOD       wavelength: 760.0
            channel: 33     label: S22_D11 760      type: DOD       wavelength: 760.0
            channel: 34     label: S19_D12 760      type: DOD       wavelength: 760.0
            channel: 35     label: S20_D12 760      type: DOD       wavelength: 760.0
            channel: 36     label: S24_D12 760      type: DOD       wavelength: 760.0
            channel: 37     label: S2_D1 850        type: DOD       wavelength: 850.0
            channel: 38     label: S4_D1 850        type: DOD       wavelength: 850.0
            channel: 39     label: S1_D1 850        type: DOD       wavelength: 850.0
            channel: 40     label: S4_D2 850        type: DOD       wavelength: 850.0
            channel: 41     label: S6_D2 850        type: DOD       wavelength: 850.0
            channel: 42     label: S3_D2 850        type: DOD       wavelength: 850.0
            channel: 43     label: S2_D3 850        type: DOD       wavelength: 850.0
            channel: 44     label: S4_D3 850        type: DOD       wavelength: 850.0
            channel: 45     label: S7_D3 850        type: DOD       wavelength: 850.0
            channel: 46     label: S5_D3 850        type: DOD       wavelength: 850.0
            channel: 47     label: S4_D4 850        type: DOD       wavelength: 850.0
            channel: 48     label: S6_D4 850        type: DOD       wavelength: 850.0
            channel: 49     label: S7_D4 850        type: DOD       wavelength: 850.0
            channel: 50     label: S8_D4 850        type: DOD       wavelength: 850.0
            channel: 51     label: S10_D5 850       type: DOD       wavelength: 850.0
            channel: 52     label: S9_D5 850        type: DOD       wavelength: 850.0
            channel: 53     label: S12_D6 850       type: DOD       wavelength: 850.0
            channel: 54     label: S14_D6 850       type: DOD       wavelength: 850.0
            channel: 55     label: S11_D6 850       type: DOD       wavelength: 850.0
            channel: 56     label: S10_D7 850       type: DOD       wavelength: 850.0
            channel: 57     label: S15_D7 850       type: DOD       wavelength: 850.0
            channel: 58     label: S13_D7 850       type: DOD       wavelength: 850.0
            channel: 59     label: S12_D8 850       type: DOD       wavelength: 850.0
            channel: 60     label: S14_D8 850       type: DOD       wavelength: 850.0
            channel: 61     label: S16_D8 850       type: DOD       wavelength: 850.0
            channel: 62     label: S17_D9 850       type: DOD       wavelength: 850.0
            channel: 63     label: S18_D9 850       type: DOD       wavelength: 850.0
            channel: 64     label: S21_D9 850       type: DOD       wavelength: 850.0
            channel: 65     label: S17_D10 850      type: DOD       wavelength: 850.0
            channel: 66     label: S18_D10 850      type: DOD       wavelength: 850.0
            channel: 67     label: S23_D10 850      type: DOD       wavelength: 850.0
            channel: 68     label: S19_D11 850      type: DOD       wavelength: 850.0
            channel: 69     label: S22_D11 850      type: DOD       wavelength: 850.0
            channel: 70     label: S19_D12 850      type: DOD       wavelength: 850.0
            channel: 71     label: S20_D12 850      type: DOD       wavelength: 850.0
            channel: 72     label: S24_D12 850      type: DOD       wavelength: 850.0
        
View optode locations:
```julia
p = plot_locs(n, src_labels=false, det_labels=false, opt_labels=false, plot_size=600, head=false)
plot_save(p, file_name="nirs-locs.png")
```
![](images/nirs-locs.png)

(!) Red (or black in monochrome mode) optodes represent sources, green (white) optodes are detectors.

Get wavelengths:
```julia
n.header.recording[:wavelengths]
```

Output:

    2-element Vector{Float64}:
     760.0
     850.0

Get channel of 760 or 850 nm wavelength:
```julia
ch760 = get_channel_bywl(n, wl=760)
ch850 = get_channel_bywl(n, wl=850)

d760 = n.data[ch760, :, :]
d850 = n.data[ch850, :, :]
```

Convert RAW intensity to OD (optical density):
```julia
n = import_nirs("test/files/fnirs-test-nirs.nirs")
intensity2od!(n)
```

(!) OD channels will be added to the object as `nirs_od` channel type. OD channels will be placed after RAW (intensity channels) and before other channels.

Convert OD (optical density) to hemoglobin concentration:
```julia
n = import_nirs("test/files/fnirs-test-nirs.nirs")
intensity2od!(n)
od2conc!(n)
```

(!) For each channel three channels will be added: `nirs_hbo` (oxygenated hemoglobin), `nirs_hbr` (deoxgenated hemoglobin) and `nirs_hbt` (total hemoglobin). The sequence of added channels is: ch1 HbO, ch1 HbR, ch1 HbT, ch2 HbO, ch2 HbR, ch2 HbT etc.

(!) It is recommended to run a band-pass filter prior to calculating Hb concentration:
```julia
n = import_nirs("test/files/fnirs-test-nirs.nirs")
intensity2od!(n)
# OD of channel 1
ch1_od = n.data[5, :, :]
NeuroAnalyzer.filter!(n, fprototype=:fir, ftype=:bp, cutoff=(0.01, 0.5))
od2conc!(n)
# HbO, HbR, HbT of channel 1
n.header.recording[:channel_type][17:22]
ch1_od = n.data[17:22, :, :]
# do not plot AUX channels
p = NeuroAnalyzer.plot(n, seg=(1, signal_len(n)), ch=setdiff(1:22, 9:16))
plot_save(p, file_name="images/nirs1.png")
```

![](images/nirs1.png)

Plot Hb concentrations:
```julia
p1 = NeuroAnalyzer.plot(n, ch=[17, 20], type=:butterfly, seg=(1, signal_len(n)), title="HbO", ylabel="HbO [μM/mm]", xlabel="")
p2 = NeuroAnalyzer.plot(n, ch=[18, 21], type=:butterfly, seg=(1, signal_len(n)), title="HbR", ylabel="HbR [μM/mm]", xlabel="")
p3 = NeuroAnalyzer.plot(n, ch=[19, 22], type=:butterfly, seg=(1, signal_len(n)), title="HbT", ylabel="HbT [μM/mm]")
p = Plots.plot(p1, p2, p3, layout=(3,1), legend=:outertopright)
plot_save(p, file_name="images/nirs-hbo-hbr.png")
```

![](images/nirs-hbo-hbr.png)