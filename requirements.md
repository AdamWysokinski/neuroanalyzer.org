---
title: NeuroAnalyzer requirements
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer requirements

## Software

Wherever possible, NeuroAnalyzer will be 100% Julia based.

NeuroAnalyzer is developed and tested under Linux. Tests are also run on ARM64 (Raspberry Pi), macOS (x86 and M1/2/3 CPUs) and Windows 10 and 11. Other systems capable of running Julia (e.g. FreeBSD) should also work, but are not tested (therefore, reports from users of those systems are highly welcomed).

Linux (e.g. [Debian](http://www.debian.org)) is recommended for the best system performance.

Since the Julia language is actively developed, Julia [current stable version](https://julialang.org/downloads/#current_stable_release) is required, as NeuroAnalyzer is only tested against it. 

All required packages will be installed automatically. If required, external open-source applications may be called for certain tasks:

- exporting EEG to hdf5 objects may require system-wide hdf5 binaries, e.g. in Debian/Ubuntu: `sudo apt-get install hdf5-tools`.
- plots require several packages in the Linux systems, install these using your distribution package manager, e.g. in Debian/Ubuntu: `sudo apt-get install libxt6 libxrender1 libxext6 libgl1-mesa-glx libqt5widgets5`.

---

## Hardware

For normal processing requirements, the following hardware is recommended:

- At least quad core CPU; NeuroAnalyzer uses multi-threading for processing data, therefore multi-core CPU is highly recommended (AMD CPUs have best price-best performance ratio, e.g. AMD Threadripper Pro 5995WX, Threadripper 5975WX, Ryzen 9 5950X, Ryzen 9 5900X or Ryzen 5 5600X)
- At least 8 Gb of RAM (≥ 16 Gb of RAM is recommended for larger datasets)
- Hard drive space depending on the size of your datasets (SSD drives are recommended for data and HDD for backups) 

Large processing pipelines may benefit from running on clustered computers. Julia offers great native support for distributed and parallel processing.

GPU processing using CUDA is available for NVIDIA cards. Use `na_info()` to check if CUDA is supported on your system. AMD Radeon/Intel ARC/Apple Metal accelerators will be supported in the future.

Note for Raspberry Pi users: the default swap size is 100M, which is too small. The configuration file for swap size is: `/etc/dphys-swapfile`. The swap size is set here: `CONF_SWAPSIZE=100` (size is given in MB). If you want to change the size, you need to modify the number and restart dphys-swapfile: `/etc/init.d/dphys-swapfile restart`.
