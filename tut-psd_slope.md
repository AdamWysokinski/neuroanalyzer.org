---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Calculate and plot PSD slope

Load EEG object:
```julia
eeg = import_edf("test/eeg-test-edf.edf")
channel = 1
epoch = 1
```

Calculate PSD data using Welch periodogram:
```julia
p, f = psd(eeg, db=true)
```

Draw scatter plot of PSD data:
```julia
scatter(f, p[channel, :], ms=1, legend=false)
```

Calculate and plot PSD slope:
```julia
f, ps, frq = psd_slope(eeg, db=true)
plot!(frq, f[channel, :, epoch], title="slope = $(round(ps[channel, epoch], digits=2))")
```

Calculate PSD slope of the alpha band:
```julia
f, ps, frq = psd_slope(eeg, frq_lim=(8, 14), db=true)
```