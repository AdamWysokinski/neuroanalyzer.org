---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: Plot (1)

Plot multi-channel signal:
```julia
p = NeuroAnalyzer.plot(eeg)
plot_save(p, file_name="images/eeg_mch.png")
```
![](images/eeg_mch.png)

Plot single-channel, using time-points:
```julia
p = NeuroAnalyzer.plot(eeg, ch=1, seg=(1+10*sr(eeg), 12*sr(eeg)))
plot_save(p, file_name="images/eeg_ch1.png")
```
![](images/eeg_ch1.png)

Plot multi-channel signal, mean ± 95CI:
```julia
p = NeuroAnalyzer.plot(eeg, ch=3:4, type=:mean)
plot_save(p, file_name="images/eeg_ch34avg.png")
```
![](images/eeg_ch34avg.png)

Butterfly plot multi-channel signal:
```julia
p = NeuroAnalyzer.plot(eeg, ch=1:4, type=:butterfly)
plot_save(p, file_name="images/eeg_ch34bf.png")
```
![](images/eeg_ch34bf.png)

You may also compare two signals, e.g. before and after filtering:
```julia
eeg1 = import_edf("NEURO-testing-data/EDF/eeg-test-edf.edf");
eeg2 = NeuroAnalyzer.filter(eeg1, fprototype=:fir, ftype=:lp, cutoff=30)
p = NeuroAnalyzer.plot(eeg1, eeg2, ch=1:19)
plot_save(p, file_name="images/plot_twosignals.png")
```

![](images/plot_twosignals.png)

(!) The first signal ("before") is drawn in black, the second ("after") -- in red.

Alternatively, interactive preview is also available:
```julia
iview(eeg1, eeg2, zoom=20)
```

![](images/preview_2signals.png)

(!) Defaut `zoom` value is 5 seconds.

Keyboard shortcuts:

- `ctrl-a`: go to the signal beginning
- `ctrl-s`: go to the signal end
- `ctrl-z`: go back by 1 seconds
- `ctrl-x`: go forward by 1 seconds
- `ctrl-c`: go back by `zoom` seconds
- `ctrl-v`: go forward by `zoom` seconds
- `ctrl-b`: slide channels up
- `ctrl-n`: slide channels down
- `ctrl-h`: show keyboard shortcuts
- `ctrl-q`: close window
