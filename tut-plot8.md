---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: topographical maps

```julia
p = plot_topo(eeg)
plot_save(p, file_name="images/plot_topo1.png")
```

![](images/plot_topo1.png)


To generate a small topomap:
```julia
p = plot_topo(eeg, large=false)
plot_save(p, file_name="images/plot_topo2.png")
```

![](images/plot_topo2.png)

Several interpolation methods are available:

- `:sh`: Shepard -- default
- `:mq`: Multiquadratic
- `:imq`: Inverse Multiquadratic
- `:tp`: ThinPlate
- `:nn`: NearestNeighbour
- `:ga`: Gaussian

```julia
p = plot_topo(eeg, imethod=:sh, title="Shepard")
plot_save(p, file_name="images/plot_topo3.png")
p = plot_topo(eeg, imethod=:mq, title="Multiquadratic")
plot_save(p, file_name="images/plot_topo4.png")
p = plot_topo(eeg, imethod=:imq, title="Inverse Multiquadratic")
plot_save(p, file_name="images/plot_topo5.png")
p = plot_topo(eeg, imethod=:tp, title="ThinPlate")
plot_save(p, file_name="images/plot_topo6.png")
p = plot_topo(eeg, imethod=:nn, title="NearestNeighbour")
plot_save(p, file_name="images/plot_topo7.png")
p = plot_topo(eeg, imethod=:ga, title="Gaussian")
plot_save(p, file_name="images/plot_topo8.png")
```

All plots show topomap of the signal averaged over time window 0:10 s.

![](images/plot_topo3.png)
![](images/plot_topo4.png)
![](images/plot_topo5.png)
![](images/plot_topo6.png)
![](images/plot_topo7.png)
![](images/plot_topo8.png)

# Plot weights

```julia
p = plot_weights(eeg, connections=rand(-1:0.1:1, 19), ch=1:19)
plot_save(p, file_name="images/plot_weights.png")
```

![](images/plot_weights.png)

# Plot connections

```julia
p = plot_connections(eeg, connections=rand(19, 19), ch=1:19, threshold=0.5)
plot_save(p, file_name="images/plot_connections.png")
```

![](images/plot_connections.png)