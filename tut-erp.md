---
title: NeuroAnalyzer tutorials
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer tutorials: ERPs

ERP is created from epoched `NeuroAnalyzer.NEURO` object via `erp()` command. ERP object has `OBJ.header.recording[:data_type]` set to "erp". First epoch of the OBJ is ERP, other epochs are the original data.

```julia
eeg = import_recording("NEURO-testing-data/Brainvision/s1_faces.vhdr")
```

Output:

    [ Info: Imported: < EEG, 34 × 424488 × 1 (829.078125 s) > 

Pre-process:
```
load_locs!(eeg, file_name="locs/standard-10-20-cap81.locs")
NeuroAnalyzer.filter!(eeg, fprototype=:butterworth, ftype=:hp, cutoff=1, order=8)
NeuroAnalyzer.filter!(eeg, fprototype=:iirnotch, cutoff=50, bw=2)
reference_avg!(eeg)
```

```
view_marker(eeg)
```

Output:

    n    ID                      start [s]   length [s]  description             channel
    1    'New Segment'           0.0         0.0         'marker'                0
    2    'Marker'                0.0         0.0         'Impedance'             0
    3    'Stimulus'              20.562      0.0         's111'                  0
    4    'Stimulus'              20.98       0.0         's130'                  0
    5    'Stimulus'              21.826      0.0         's71'                   0
        ...
    400  'Stimulus'              799.207     0.0         's130'                  0
    401  'Stimulus'              799.986     0.0         's71'                   0
    402  'Stimulus'              803.119     0.0         's130'                  0
    403  'Stimulus'              803.65      0.0         's71'                   0
    404  'Stimulus'              806.529     0.0         's121'                  0
    405  'Stimulus'              806.574     0.0         's122'                  0
    406  'Stimulus'              806.619     0.0         's102'                  0

Epoch by marker "s71":
```
eeg_epoched = epoch(eeg, marker="s71", offset=0.5, ep_len=2.5)
p = plot(eeg_epoched)
```

Create ERP, set baseline from -500 to 0 msec:
```
erp_s71 = erp(eeg_epoched, bl=(-0.5, 0))
p = plot_erp(erp_s71, ch=10)
plot_save(p, file_name="images/erp1.png")
```

(!) Baseline segment time points must be given in seconds.

By default, baseline is subtracted after averaging, to subtract baseline prior to averaging:
```julia
erp_s71 = average_epochs(eeg_epoched, bl=(-0.5, 0), blfirst=true)
```

Output: 

    [ Info: Positive peak time: 143.0 ms
    [ Info: Positive peak amplitude: 5.15 μV
    [ Info: Negative peak time: 234.0 ms
    [ Info: Negative peak amplitude: -8.82 μV

![](images/erp1.png)

(!) To reverse the Y axis (so that negative values are at the top), use the `yrev=true` option:

```julia
p = plot_erp(erp_s71, ch=10, yrev=true)
plot_save(p, file_name="images/erp_rev.png")
```

![](images/erp_rev.png)

To add the response time line to the plot:
```julia
p = plot_erp(erp_s71, rt=0.18)
plot_save(p, file_name="images/erp_rt.png")
```

![](images/erp_rt.png)

Detect peaks:
```julia
erp_peaks(erp_s71)
```

(!) The output matrix contains the position (in samples) of the positive (first column) and negative (second column) peaks.

Butterfly plot over channels:
```julia
p = plot_erp(erp_s71, ch=1:4, type=:butterfly, avg=true)
plot_save(p, file_name="images/erp2.png")
```

![](images/erp2.png)

Butterfly plot over epochs:
```julia
p = plot_erp(erp_s71, ch=1, type=:butterfly, avg=true)
plot_save(p, file_name="images/erp3.png")
```

![](images/erp3.png)

Mean plot over channels:
```julia
p = plot_erp(erp_s71, ch=1:30, type=:mean, peaks=false)
plot_save(p, file_name="images/erp4.png")
```

![](images/erp4.png)

Mean plot over epochs:
```julia
p = plot_erp(erp_s71, ch=1, type=:mean, peaks=false)
plot_save(p, file_name="images/erp5.png")
```

![](images/erp5.png)

Topographical plot of ERPs:
```julia
p = plot_erp(erp_s71, ch=1:30, type=:topo)
plot_save(p, file_name="images/erp6.png")
```

![](images/erp6.png)

Positive peak is at 143 ms, negative peak at 234 ms (see `plot_erp()` output). Draw markers and topoplots at these time points:
```julia
p1 = plot_erp(erp_s71, ch=signal_channels(erp_s71), type=:butterfly, labels=false, tm=[143, 234], title="", top_margin=10Plots.px, channel_labels=false)
p2 = plot_topo(erp_s71, ch=signal_channels(erp_s71), seg=(0.143, 0.144), cb=false, title="")
p3 = plot_topo(erp_s71, ch=signal_channels(erp_s71), seg=(0.234, 0.235), cb=false, title="")
c1 = resize_canvas(plot2canvas(p2), r=0.2)
c2 = resize_canvas(plot2canvas(p3), r=0.2)
c = add_topmargin_canvas(plot2canvas(p1), c1)
c = add_to_canvas(c, c1, x=230, y=0, title="143 ms", view=false)
c = add_to_canvas(c, c2, x=395, y=0, title="234 ms", view=false)
iview_plot(c)
```

![](images/erp7.png)

Plot stacked channels:
```julia
p = plot_erp(erp_s71, ch=signal_channels(erp_s71), type=:stack)
plot_save(p, file_name=file_name="images/erp8.png")
```

![](images/erp8.png)

Plot stacked epochs:
```julia
p = plot_erp(erp_s71, ch=10, type=:stack, tm=[143, 234])
plot_save(p, file_name=file_name="images/erp9.png")
```

![](images/erp9.png)

To plot stacked epochs and ERP:
```julia
p1 = plot_erp(erp_s71, ch=1, type=:stack, cb=false, title="", xlabel="", xticks=false)
p2 = plot_erp(erp_s71, ch=1, title="")
p = Plots.plot(p1, p2, layout=@layout [a{0.8h}; b{0.2h}])
plot_save(p, file_name=file_name="images/erp_stack.png")
```

![](images/erp_stack.png)

To plot smoothed stacked epochs and ERP:
```julia
p = plot_erp(erp_s71, ch=1, type=:stack, smooth=true, n=5)
plot_save(p, file_name=file_name="images/erp_smooth.png")
```

![](images/erp_smooth.png)

To plot the response time line over the ERP stack plot:
```julia
p = plot_erp(erp_s71, ch=1, type=:stack, rt=rt)
plot_save(p, file_name=file_name="images/erp_rt1.png")
```

![](images/erp_rt1.png)

To plot the response time line over the ERP stack plot and sort epochs by RT values:
```julia
p = plot_erp(erp_s71, ch=1, type=:stack, rt=rt, sort_epochs=true)
plot_save(p, file_name=file_name="images/erp_rt2.png")
```

![](images/erp_rt2.png)

Calculate amplitude at given time point:
```julia
amp_at(erp_s71, t=0.250)
```

Calculate average, minimum and maximum amplitude over a time segment:
```julia
avgamp_at(erp_s71, t=(0.150, 0.250))
minamp_at(erp_s71, t=(0.150, 0.250))
maxamp_at(erp_s71, t=(0.150, 0.250))
```

Calculate non-phase-locked signal (signal - ERP):
```julia
erp_s71_npl = npl(erp_s71)
```

Calculate EROs (Event-Related Oscillations) spectrogram:
```julia
s, f, t = eros(erp_s71, ch=10)
p = plot_eros(s, f, t, tm=[143, 234], frq_lim=(0, 40))
plot_save(p, file_name=file_name="images/eros.png")
```
![](images/eros.png)

Calculate EROp (Event-Related Oscillations) power-spectrum:
```julia
p, f = erop(erp_s71, ch=10)
p = plot_erop(p, f, frq_lim=(0, 40))
plot_save(p, file_name=file_name="images/erop.png")
```
![](images/erop.png)