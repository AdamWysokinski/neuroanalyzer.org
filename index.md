---
title: NeuroAnalyzer
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# Welcome to the NeuroAnalyzer website

This is the home of [NeuroAnalyzer.jl](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl) documentation and tutorials.

NeuroAnalyzer is a [Julia](https://julialang.org) toolbox for analyzing neurophysiological data. Currently it covers importing, editing, processing, visualizing, and analyzing EEG, MEP and EDA data. Preliminary functionality is also available for MEG, NIRS, ECoG, SEEG and iEEG recordings.

Various methods for modeling non-invasive brain stimulation protocols (tDCS/tACS/tRNS/tPCS/TMS/TUS/INS) will also be implemented (NeuroStim submodule). Another submodule, NeuroTester, will allow designing and running psychological studies. Certain neurophysiological data can be recorded using NeuroRecorder submodule.

NeuroAnalyzer contains a set of separate (high- and low-level) functions. Some interactive graphical user interface (GUI) functions are also available. NeuroAnalyzer functions can be combined into an analysis pipeline, i.e. a Julia script containing all steps of your analysis. This, combined with processing power of Julia language and easiness of distributing calculations across computing cluster, will make NeuroAnalyzer particularly useful for processing large amounts of neurophysiological data.

NeuroAnalyzer is a collaborative, non-commercial project, developed for researchers in psychiatry, neurology and neuroscience.

NeuroAnalyzer repository is located at Codeberg [AdamWysokinski/NeuroAnalyzer.jl](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl) and mirrored at Github [JuliaHealth/Neuroanalyzer.jl](https://github.com/JuliaHealth/NeuroAnalyzer.jl).

---

# What you can do with NeuroAnalyzer

Currently NeuroAnalyzer is focused on resting-state analysis. Some ERP functions are already available, while other type of analyses will be developed in future versions. The goal is to make a powerful, expandable and flexible environment for processing neurophysiological data.

1. Load neurophysiological recordings:
    - EEG (EDF, EDF+, BDF, BDF+, GDF, Alice4, DigiTrack, BrainVision, CSV, FieldTrip, EEGLAB, NPY, Thymatron, NCS, CNT, XDF)
    - MEG (FIFF, FieldTrip)
    - NIRS (SNIRF, NIRS, NIRX, FieldTrip)
    - MEP (DuoMAG)
    - body sensors: acceleration, magnetic field, angular velocity and orientation
    - electrode positions (CED, LOCS, ELC, TSV, SFP, CSD, GEO, MAT, TXT, DAT, ASC)
2. Edit:
    - edit channel data (unit, type, label)
    - edit location data (electrode location)
    - trim (remove part of the signal)
    - resample (up/down)
    - divide into epochs (fixed and by event markers)
    - delete channels/epochs
    - auto-detect bad channels/epochs
    - interpolate channels (planar interpolation/linear regression)
4. Process:
    - reference (common/averaged/auricular/mastoid/Laplacian/custom montage)
    - filter (FIR/IIR/Remez/moving average/moving median/polynomial filters), all types (HP, LP, BP, BS); with preview of filter response
    - remove power line noise
    - auto-detect and remove electrode pops
    - ICA decompose/reconstruct
    - PCA
    - convolution (in time and frequency domain)
    - create ERP (event-related potentials)
    - NIRS: convert raw light intensity to optical density and HbO/HbR/HbT concentrations
5. Analyze:
    - signal comparison
    - stationarity
    - frequency analysis: total power, band power (absolute and relative)
    - auto- and cross- covariance and correlation (biased and unbiased)
    - time-frequency analysis: various spectrogram methods (FFT-based, short-time Fourier transform, multi-tapered periodogram, Morlet wavelet convolution, Gaussian and Hilbert transform, continuous wavelet transformation)
    - coherence and magnitude-squared coherence
    - mutual information
    - entropy, negentropy
    - envelopes (amplitude, power, spectrogram)
    - power spectrum slope
    - PLI/ISPC/ITPC
    - ERP: detect peaks, analyze amplitude and average amplitude
    - EROs (event-related oscillations): spectrogram, power spectrum
    - HRV (heart rate variability): time-domain analysis (MENN, MDNN, VNN, SDNN, RMSSD, SDSD, NN50, pNN50, NN20, pNN20)
    - MEPs: detect peaks, analyze amplitude and average amplitude
6. Plot:
    - signal (single-/multi-channel)
    - power spectrum (single-/multi-channel, 2D/3D)
    - spectrogram (single-/multi-channel)
    - topographical maps
    - weights at channel locations
    - inter-channel connections
    - matrices (channel × channel)
    - channel/epoch data (histogram/bar plot/dot plot/box plot/violin plot/polar plot/paired data)
    - ERPs: amplitude, topographical distribution
    - EROs: spectrogram, power spectrum
    - MEPs: amplitude
    - interactive plots (amplitude, signal, spectrum, spectrogram, ICA, topographical map, channel details)

All computations are performed using the double-precision 64-bit floating point format. NeuroAnalyzer data is stored using standard Julia Array and can be easily exported as DataFrame. Thus, external processing of those data using Julia packages is readily available.

NeuroAnalyzer also includes NeuroRecorder, a set of functions for recording various neurophysiological signals:

- **Electrodermal Activity** (EDA) = Galvanic Skin Response (GSR) -- via Raspberry Pi/Arduino
- **Angular Velocity Sensors** (AVS) -- via Raspberry Pi/Arduino

NeuroTester modules implements various neuropsychological tests:

- **Finger Tapping Test** (FTT) -- using computer keyboard or external panel attached to Raspberry Pi
- **Two-point Pinch Test** (TPT) -- using finger-worn accelerator attached to Arduino

---

# Why Julia?

There are many excellent MATLAB and Python based EEG/MEG/NIRS software (e.g. EEGLAB, Fieldtrip, Brainstorm, MNE). They have been developed for many years and are well established in the scientific community. Many state-of-the-art papers were published using data prepared using these programs.

However, compared with Python and MATLAB, there are many advantages of Julia, which underlie my decision to start developing such a toolbox in Julia.

I believe that [Julia is the future of scientific computing](https://towardsdatascience.com/meet-julia-the-future-of-data-science-52414b29ebb) and [scientific data analysis](https://epubs.siam.org/doi/10.1137/141000671). Major advantages of Julia are listed in [Julia documentation](https://docs.julialang.org/en/v1/). 

1. [Julia is fast](https://dl.acm.org/doi/10.1145/3276490). In many situations Julia is considerably faster than [Python](https://programming-language-benchmarks.vercel.app/julia-vs-python) (without having to use numba/cython) and [MATLAB](https://julialang.org/benchmarks/). Moreover, Julia provides unlimited scalability. Julia programs can easily be ran on a large cluster or across distributed computers.
2. Julia is open-source and free. Increasing MATLAB licensing costs are prohibitive to individual researchers and many research institutions.
3. From its very beginning Julia is being focused on scientific computations. Currently only Julia, C, C++ and Fortran belong to the HPC (High Performance Computing) [Petaflop Club](https://www.hpcwire.com/off-the-wire/julia-joins-petaflop-club/). Julia is designed for distributed and parallel computations, making it great for distributed analyzes of large data sets. 
4. Most of the Julia packages are written in pure Julia. It's easier to understand and modify their code if you already know Julia.
5. Julia is beautifully designed, making programming in Julia a pure pleasure. This elegant design makes Julia easy to read and write.

Benchmarks against MNE and EEGLAB are available [here](benchmarks.html).

---

# What's new

Changelog and commit details are [here](changelog.html).

---

# What's next

This [roadmap](roadmap.html) of the future developments of NeuroAnalyzer is neither complete, nor in any particular order. You are encouraged to add new functions required for your study. You may also submit a feature request using Codeberg [NeuroAnalyzer.jl](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl) repository page.

---

# Hardware and software requirements

See [https://neuroanalyzer.org/requirements.html](https://neuroanalyzer.org/requirements.html) for more details.

---

# Documentation

Documentation is available in the following formats:

- [HTML](https://neuroanalyzer.org/docs)
- [Markdown](https://codeberg.org/AdamWysokinski/NeuroAnalyzer-docs/src/branch/main/Documentation.md)

---

# Known bugs

List of know, reported and fixed bugs is [here](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/issues?labels=70759).

---

# Tutorials

If you are new to Julia, please take a look at some resources for [Getting Started with Julia](https://julialang.org/learning/getting-started/).

Quickstart: add NeuroAnalyzer from the [Pkg REPL](https://docs.julialang.org/en/v1/stdlib/REPL/#Pkg-mode): `pkg> add NeuroAnalyzer`.

In Julia REPL, each NeuroAnalyzer function documentation is available via `?<function_name>`, e.g. `?plot_psd`. Since some function names are exported by other packages (e.g. `plot` by the Plots package), their use must be qualified: `NeuroAnalyzer.plot()`.

Glossary of terms specific for NeuroAnalyzer is [here](glossary.html).

1. Using NeuroAnalyzer
    1. [Download and installation](tutorials/download.html)
    2. [Generating and using sysimage](tutorials/sysimage.html)
    3. [General remarks](tutorials/general.html)
    4. [Preferences](tutorials/preferences.html)
    5. [Plugins](tutorials/plugins.html)
2. Load data and electrode positions
    1. [Load/save/import/export data](tutorials/io.html)
    2. [Load/edit/preview electrode positions](tutorials/locs.html)
3. Edit
    1. [Edit (1)](tutorials/edit1.html): copy, view/edit metadata, view properties/history
    2. [Edit (2)](tutorials/edit2.html): edit channels/epochs, using markers
    3. [Edit (3)](tutorials/edit3.html): bad channels/epochs: detect/trim signal/remove bad epochs/interpolate channels
    4. [Edit (4)](tutorials/edit4.html): resample, band frequencies
    4. [Edit (5)](tutorials/edit5.html): picks and clusters
    4. [Edit (6)](tutorials/edit6.html): virtual channels and applying formula
4. Process
    1. [Process (1)](tutorials/proc1.html): reference (common/averaged/auricular/mastoid/Laplacian/custom montage)
    2. [Process (2)](tutorials/proc2.html): filtering (FIR/IIR/Remez/moving average/moving median/polynomial/convolution)
    3. [Process (3)](tutorials/proc3.html): demean, normalize, taper, convolution, PCA
    4. [Process (4)](tutorials/proc4.html): remove electrode pops
    5. [Process (5)](tutorials/proc5.html): ICA decomposition/reconstruction
    6. [Process (6)](tutorials/proc6.html): interpolate bad channels
5. Analyze
    1. [Analyze (1)](tutorials/analyze1.html): components
    2. [Analyze (2)](tut-analyze2.html): frequency domain
    3. [Analyze (3)](tut-analyze3.html): segments
    4. [Analyze (4)](tut-analyze4.html): coherence
    5. [Analyze (5)](tut-analyze5.html): lateralization index and asymmetry index
    5. [Analyze (6)](tut-analyze6.html): miscellaneous
6. Plot
    1. [Plot (1)](tut-plot1.html): plot multi-/single-channel signal, compare two signals
    2. [Plot (2)](tut-plot2.html): plot frequency analysis (PSD: single-/multi-channel, Welch's periodogram/multi-tapered/Morlet wavelet, 2d/3d)
    3. [Plot (3)](tut-plot3.html): plot spectrogram (single-/multi-channel, regular/STFT/Morlet wavelet)
    4. [Plot (4)](tut-plot4.html): complex/tiled plots
    5. [Plot (5)](tut-plot5.html): filter response
    6. [Plot (6)](tut-plot6.html): band powers
    7. [Plot (7)](tut-plot7.html): auto/cross-correlation/covariance
    8. [Plot (8)](tut-plot8.html): topographical maps
    9. [Plot (9)](tut-plot9.html): interactive preview and edit
7. Statistic
    1. [Statistics (1)](tutorials/stat1.html): bootstrapping
8. Misc
    1. [Pipelines](tutorials/pipelines.html)
    2. [Study](tut-study.html)
    3. [Calculate and plot PSD slope](tut-psd_slope.html)
    4. [Stationarity](tut-stationarity.html)
    5. [Remove power line noise](tut-power_line.html)
    6. [Animate](tut-animate.html)
    7. [HRV](tut-hrv.html)
9. Other data types:
    1. [MEPs](tutorials/mep.html)
    2. [ERPs](tut-erp.html)
    3. [NIRS](tut-nirs.html)
    4. [ECoG](tut-ecog.html)
    5. [MEG](tut-meg.html)
10. NeuroRecorder:
    1. [EDA](tutorials/neurorecorder-eda.html)
11. NeuroTester:
    1. [FTT](tutorials/neurotester-ftt.html)
    2. [TPT](tutorials/neurotester-tpt.html)

---

# Plugins

The list of available plugins is [here](plugins.html).

Please let us know if you would like to add your plugin to the list.

---

# License

This software is licensed under [The 2-Clause BSD License](LICENSE).

---

# How to cite

DOIs for specific version numbers are provided by [Zenodo](https://zenodo.org/search?page=1&size=20&q=conceptrecid%3A%227372648%22&sort=-version&all_versions=True). To cite the current version, use:

```bibtex
@software{NeuroAnalyzer,
  author       = {Adam Wysokiński},
  title        = {NeuroAnalyzer},
  year         = 2025,
  publisher    = {Zenodo},
  version      = {0.25.3},
  doi          = {10.5281/zenodo.7372648},
  url          = {https://doi.org/10.5281/zenodo.7372648}
}
```

Version numbers are: 0.yy.m (yy: last two digits of the year, m: number of the month). Stable versions are released at the beginning of the month indicated in the version number.

---

# Financial support

If you would like to support the project financially, we have the Liberapay account:
<a href="https://liberapay.com/~1829183/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>

---

# Contributing

Every contribution (bug reports, fixes, new ideas, feature requests or additions, speed optimization, better code, documentation improvements, typos, etc.) to the project is highly welcomed.

You are very welcome to raise issues and start pull requests. Bugs, suggestions and questions should be reported using the Codeberg [AdamWysokinski/NeuroAnalyzer.jl](https://codeberg.org/AdamWysokinski/NeuroAnalyzer.jl/issues) (preferred method) or Github [JuliaHealth/NeuroAnalyzer.jl](https://github.com/JuliaHealth/NeuroAnalyzer.jl/issues) issues page.

If you notice any bugs, such as crashing code, incorrect results or speed issues, please raise a Codeberg/GitHub issue. Before filing an issue please:

- check that there are no similar existing issues already
- check that your versions are up to date

If you want to report a bug, include your version and system information, and all relevant information. If possible, condense your bug into the shortest example possible that the maintainers can replicate, a so called "minimal working example" or MWE.

If you want to suggest a new feature, for example functionality that other plotting packages offer already, include supplementary material such as example images if possible, so it's clear what you are asking for.

When opening a pull request, please add a short but meaningful description of the changes/features you implemented. Moreover, please add tests (where appropriate) to ensure that your code is working as expected.

For each feature you want to contribute, please file a separate PR to keep the complexity down and time to merge short. Add PRs in draft mode if you want to discuss your approach first.

---

# List of publications using NeuroAnalyzer

1. Wysokiński A, Szczepocka E, Szczakowska A. Improved cognitive performance, increased theta, alpha, beta and decreased delta powers after cognitive rehabilitation augmented with tDCS in a patient with post-COVID-19 cognitive impairment (brain-fog). Psychiatry Research Case Reports. 2023 DOI: [10.1016/j.psycr.2023.100164](https://doi.org/10.1016/j.psycr.2023.100164)
2. Sochal M. et al. The relationship between sleep quality measured by polysomnography and selected neurotrophic factors. Journal of Clinical Medicine. 2024 DOI: [10.3390/jcm13030893](https://doi.org/10.3390/jcm13030893)
3. Sochal M. et al. Circadian Rhythm Genes and Their Association with Sleep and Sleep Restriction. International Journal of Molecular Sciences. 2024 DOI: [10.3390/ijms251910445](https://doi.org/10.3390/ijms251910445)
4. Datseris G, Zelko J. Physiological signal analysis and open science using the Julia language and associated software. Frontiers in Network Physiology. 2024 DOI: [10.3389/fnetp.2024.1478280](https://doi.org/10.3389/fnetp.2024.1478280)

If you have used NeuroAnalyzer in your research, you are kindly asked to [report](mailto:adam.wysokinski@neuroanalyzer.org) the bibliographic description of your publication.

---

This website is maintained by [Adam Wysokiński](mailto:adam.wysokinski@neuroanalyzer.org) [![ORCID](images/orcid.png)](https://orcid.org/0000-0002-6159-6579)

[![UMED](images/umed.png)](https://en.umed.pl)