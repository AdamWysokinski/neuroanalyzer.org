---
title: NeuroAnalyzer
---

<div style="text-align: center;">
<a href="http://neuroanalyzer.org">

![](images/neuroanalyzer.png)

</a>
</div>

# NeuroAnalyzer plugins

Below is the list of available plugins.

## Plot

[plot_ISPC.jl](https://codeberg.org/AdamWysokinski/plot_ISPC.jl)

Plot ISPC (Inter-Stimulus Phase Coherence).

[plot_ITPC.jl](https://codeberg.org/AdamWysokinski/plot_ITPC.jl)

Plot ITPC (Inter-Trial-Phase Clustering).

[plot_PLI.jl](https://codeberg.org/AdamWysokinski/plot_PLI.jl)

Plot PLI (Phase Lag Index).

[plot_ENV.jl](https://codeberg.org/AdamWysokinski/plot_ENV.jl)

Plot amplitude/power spectrum/spectrogram/Hilbert spectrum amplitude envelope.
